-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 10-10-2022 a las 14:10:14
-- Versión del servidor: 10.4.24-MariaDB
-- Versión de PHP: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `pinac_db`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `activitat`
--

CREATE TABLE `activitat` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `preu` float(4,2) NOT NULL,
  `visible` float NOT NULL DEFAULT 1,
  `data` date NOT NULL,
  `inici_inscripcions` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `final_inscripcions` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `data_creacio` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `activitat_grup`
--

CREATE TABLE `activitat_grup` (
  `grup_id` int(11) NOT NULL,
  `activitat_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grup`
--

CREATE TABLE `grup` (
  `id` int(11) NOT NULL,
  `nom` text COLLATE utf8_spanish_ci NOT NULL,
  `ordre` int(11) NOT NULL,
  `visible` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `infant`
--

CREATE TABLE `infant` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `cognoms` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `data_naix` date NOT NULL,
  `telefon_altre` varchar(13) COLLATE utf8_spanish_ci NOT NULL,
  `telefon_fix` varchar(13) COLLATE utf8_spanish_ci NOT NULL,
  `telefon_mare` varchar(13) COLLATE utf8_spanish_ci NOT NULL,
  `telefon_pare` varchar(13) COLLATE utf8_spanish_ci NOT NULL,
  `data_alta` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `infant_activitat`
--

CREATE TABLE `infant_activitat` (
  `infant_id` int(11) NOT NULL,
  `activitat_id` int(11) NOT NULL,
  `data_inscripció` timestamp NOT NULL DEFAULT current_timestamp(),
  `tutor_id` int(11) NOT NULL COMMENT 'El tutor que ha registrat l''infant'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `infant_grup`
--

CREATE TABLE `infant_grup` (
  `infant_id` int(11) NOT NULL,
  `grup_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `infant_provisional`
--

CREATE TABLE `infant_provisional` (
  `nom` varchar(255) NOT NULL,
  `cognoms` varchar(255) NOT NULL,
  `data_naix` date NOT NULL,
  `telefon_altre` varchar(13) NOT NULL,
  `telefon_fix` varchar(13) NOT NULL,
  `telefon_mare` varchar(13) NOT NULL,
  `telefon_pare` varchar(13) NOT NULL,
  `remesaId` varchar(50) NOT NULL COMMENT 'Controla el bloc de dates que s''estan entrant i el seu autoborrat.',
  `action` varchar(30) NOT NULL,
  `grupId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `infant_tutor`
--

CREATE TABLE `infant_tutor` (
  `infant_id` int(11) NOT NULL,
  `tutor_id` int(11) NOT NULL,
  `data_vinculacio` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `login_token`
--

CREATE TABLE `login_token` (
  `id` int(11) NOT NULL,
  `tutor_id` int(11) NOT NULL,
  `token` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `data_expira` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tutor`
--

CREATE TABLE `tutor` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `cognoms` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `dni` varchar(9) COLLATE utf8_spanish_ci NOT NULL,
  `data_naix` date NOT NULL,
  `correu` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `data_alta` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuari`
--

CREATE TABLE `usuari` (
  `id` int(11) NOT NULL,
  `nom` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `contrasenya` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `activitat`
--
ALTER TABLE `activitat`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `activitat_grup`
--
ALTER TABLE `activitat_grup`
  ADD PRIMARY KEY (`grup_id`,`activitat_id`),
  ADD KEY `grup_id` (`grup_id`,`activitat_id`),
  ADD KEY `activitat_id` (`activitat_id`);

--
-- Indices de la tabla `grup`
--
ALTER TABLE `grup`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `infant`
--
ALTER TABLE `infant`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `infant_activitat`
--
ALTER TABLE `infant_activitat`
  ADD PRIMARY KEY (`infant_id`,`activitat_id`),
  ADD KEY `infant_id` (`infant_id`,`activitat_id`),
  ADD KEY `tutor_registre` (`tutor_id`),
  ADD KEY `activitat_id` (`activitat_id`);

--
-- Indices de la tabla `infant_grup`
--
ALTER TABLE `infant_grup`
  ADD PRIMARY KEY (`infant_id`,`grup_id`),
  ADD KEY `grup_id` (`grup_id`);

--
-- Indices de la tabla `infant_tutor`
--
ALTER TABLE `infant_tutor`
  ADD PRIMARY KEY (`infant_id`,`tutor_id`),
  ADD KEY `infant_id` (`infant_id`,`tutor_id`),
  ADD KEY `tutor_id` (`tutor_id`);

--
-- Indices de la tabla `login_token`
--
ALTER TABLE `login_token`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tutor_id` (`tutor_id`);

--
-- Indices de la tabla `tutor`
--
ALTER TABLE `tutor`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuari`
--
ALTER TABLE `usuari`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `activitat`
--
ALTER TABLE `activitat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `grup`
--
ALTER TABLE `grup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `infant`
--
ALTER TABLE `infant`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `login_token`
--
ALTER TABLE `login_token`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tutor`
--
ALTER TABLE `tutor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `usuari`
--
ALTER TABLE `usuari`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `activitat_grup`
--
ALTER TABLE `activitat_grup`
  ADD CONSTRAINT `activitat_grup_ibfk_1` FOREIGN KEY (`activitat_id`) REFERENCES `activitat` (`id`),
  ADD CONSTRAINT `activitat_grup_ibfk_2` FOREIGN KEY (`grup_id`) REFERENCES `grup` (`id`);

--
-- Filtros para la tabla `infant_activitat`
--
ALTER TABLE `infant_activitat`
  ADD CONSTRAINT `infant_activitat_ibfk_1` FOREIGN KEY (`activitat_id`) REFERENCES `activitat` (`id`),
  ADD CONSTRAINT `infant_activitat_ibfk_2` FOREIGN KEY (`infant_id`) REFERENCES `infant` (`id`),
  ADD CONSTRAINT `infant_activitat_ibfk_3` FOREIGN KEY (`tutor_id`) REFERENCES `tutor` (`id`);

--
-- Filtros para la tabla `infant_grup`
--
ALTER TABLE `infant_grup`
  ADD CONSTRAINT `infant_grup_ibfk_1` FOREIGN KEY (`grup_id`) REFERENCES `grup` (`id`),
  ADD CONSTRAINT `infant_grup_ibfk_2` FOREIGN KEY (`infant_id`) REFERENCES `infant` (`id`);

--
-- Filtros para la tabla `infant_tutor`
--
ALTER TABLE `infant_tutor`
  ADD CONSTRAINT `infant_tutor_ibfk_1` FOREIGN KEY (`infant_id`) REFERENCES `infant` (`id`),
  ADD CONSTRAINT `infant_tutor_ibfk_2` FOREIGN KEY (`tutor_id`) REFERENCES `tutor` (`id`);

--
-- Filtros para la tabla `login_token`
--
ALTER TABLE `login_token`
  ADD CONSTRAINT `login_token_ibfk_1` FOREIGN KEY (`tutor_id`) REFERENCES `tutor` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
