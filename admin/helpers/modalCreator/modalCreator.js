var defaultModalContent =
  '<span class="modalTitol">El títol</span>' +
  '<div class="modalBody">' +
  '<div class="modalText">' +
  '</div>' +
  '<div class="modalButtons"></div>' +
  '</div>';

var inputsModalContent =
  '<span class="modalTitol">El títol</span>' +
  '<div class="modalBody">' +
  '<div class="modalText">' +
  '</div>' +
  '<form class="modalInputs" method="post"></form>' +
  '<div class="modalButtons"></div>' +
  '</div>';

//Cada Botó s'haurà de passar com a paràmetre en forma d'array dins d'una array [['Text', 'onClickEvent','Class'], ...]
function openModalDefault(titol, text, botons) {
  $(".modalInfo").html(defaultModalContent);
  $(".modalTitol").html(titol);
  $(".modalText").html(text);
  buttonsHtml = "";
  botons.forEach(function (e) {
    buttonsHtml += "<button class=\"" + e[2] + "\" id=\"modalButton\" onclick=\"" + e[1] + "\"> " + e[0] + " </button>";
  });
  $(".modalButtons").html(buttonsHtml);
  $(".modalHolder").fadeIn(100);
}

//Cada Input s'haurà de passar com a paràmetre en forma d'array dins d'una array [['Label', 'Nom i Id Input','required o altres camps','Type','Placeholder','Value'], ...]
function openModalInputs(titol, text, inputs, botons) {
  $(".modalInfo").html(inputsModalContent);
  $(".modalTitol").html(titol);
  $(".modalText").html(text);
  inputsHtml = "";
  inputs.forEach(function (e) {
    if(e[3]!="hidden"){
      inputsHtml += "<span class=\"modalInputLabel\">" + e[0] + "</span>"
    }
    if(e[3]=="select"){
      selected=e[5];
      inputsHtml+= "<select id=\"" + e[1] + "\" name=\"" + e[1] + "\">"
      e[4].forEach(function (f) {
        if(selected==f[0]){
          inputsHtml += "<option selected value=\""+ f[0] +"\">"+ f[1] +"</option>";
        }else{
          inputsHtml += "<option value=\""+ f[0] +"\">"+ f[1] +"</option>";
        }
      });
      inputsHtml+= "<select/>"
    }else if(e[3]=="file"){
      inputsHtml += "<input " + e[2] + " type=\"" + e[3] + "\" id=\"" + e[1] + "\" name=\"" + e[1] + "\" placeholder=\"" + e[4] + "\" accept=\"" + e[5] + "\" />";
    }else{
      inputsHtml += "<input " + e[2] + " type=\"" + e[3] + "\" id=\"" + e[1] + "\" name=\"" + e[1] + "\" placeholder=\"" + e[4] + "\" value=\"" + e[5] + "\" />";
    }
  });
  buttonsHtml = "";
  botons.forEach(function (e) {
    buttonsHtml += "<button class=\"" + e[2] + "\" id=\"modalButton\" onclick=\"" + e[1] + "\"> " + e[0] + " </button>";
  });
  $(".modalInputs").html(inputsHtml);
  $(".modalButtons").html(buttonsHtml);
  $(".modalHolder").fadeIn(100);
}

function modalInputsGetValues() {
  valTable = [];
  $('.modalContent .modalInputs input').each(function () {
    valTable.push([this.name, this.value]);
  });
  $('.modalContent .modalInputs select').each(function () {
    valTable.push([this.name, this.value]);
  });
  return valTable;
}

function addModalHolder(tag) {
  $content =
    '<div class="modalCreator modalHolder" style="display: none;">' +
    '<div class="modalBack" onClick="hideModal()"></div>' +
    '<div class="modalContent">' +
    '<svg onClick="hideModal()" version="1.1" class="modalClose" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 94.926 94.926" style="enable-background:new 0 0 94.926 94.926;" xml:space="preserve"><g><path d="M55.931,47.463L94.306,9.09c0.826-0.827,0.826-2.167,0-2.994L88.833,0.62C88.436,0.224,87.896,0,87.335,0c-0.562,0-1.101,0.224-1.498,0.62L47.463,38.994L9.089,0.62c-0.795-0.795-2.202-0.794-2.995,0L0.622,6.096c-0.827,0.827-0.827,2.167,0,2.994l38.374,38.373L0.622,85.836c-0.827,0.827-0.827,2.167,0,2.994l5.473,5.476c0.397,0.396,0.936,0.62,1.498,0.62s1.1-0.224,1.497-0.62l38.374-38.374l38.374,38.374c0.397,0.396,0.937,0.62,1.498,0.62s1.101-0.224,1.498-0.62l5.473-5.476c0.826-0.827,0.826-2.167,0-2.994L55.931,47.463z"/></g></svg>' +
    '<div class="modalInfo"></div>' +
    '</div>' +
    '</div>';
  $(tag).append($content);
}

function hideModal() {
  $(".modalCreator.modalHolder").fadeOut(100);
}
