<?php 

session_start();
unset($_SESSION['user']);
unset($_SESSION['password']);
unset($_SESSION['expire_time']);
header("Location: ./login.php");
