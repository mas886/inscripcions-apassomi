<?php require_once("header.php");

if (isset($_GET['id']) && $_GET['id'] != '' && is_numeric($_GET['id'])) {
    $query = "SELECT * FROM `grup` WHERE id=:id";
    $activitat = getQuery($query, [":id" => $_GET['id']]);
    if (!$activitat) {
        echo ("<script>window.location.href = \"./grups.php\";</script>");
        exit;
    }
} else {
    echo ("<script>window.location.href = \"./grups.php\";</script>");
    exit;
}

if (isset($_POST['nom']) && isset($_POST['ordre']) && isset($_POST['visible'])) {
    $query = "UPDATE `grup` SET `nom`=:nom ,`ordre`= :ordre, `visible` = :visible WHERE `id`= :idGrup";
    $resGuardat = executeQuery($query, [':nom' => $_POST['nom'], ':ordre' => $_POST['ordre'], ':visible' => $_POST['visible'], ':idGrup' => $_GET['id']]);
}

$query = "SELECT * FROM `grup` WHERE id = :id";
$infoGrup = getQuery($query, [':id' => $_GET['id']])[0];

$query = "SELECT `id`, `nom`, `cognoms` FROM `infant_grup` ig JOIN `infant` i ON i.id = ig.infant_id WHERE grup_id = :idGrup ORDER BY nom";
$participants = getQuery($query, [':idGrup' => $_GET['id']]);

$query = "SELECT * FROM `activitat_grup` JOIN `activitat` a ON a.id = `activitat_id` WHERE grup_id = :idGrup ORDER BY a.id DESC";
$activitats = getQuery($query, [':idGrup' => $_GET['id']]);

?>

<!-- End Navbar -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4">
                <div class="card ">
                    <div style="margin:0px;" class="card-header row">
                        <div class="col-12">
                            <h4 class="card-title">Participants</h4>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="col-12 row">
                            <div class="col-3">
                                <i class="nc-icon nc-single-02 iconBig"></i>
                            </div>
                            <div class="col-9">
                                <span id="participantsNum" class="numberDisplay"><?php echo count($participants); ?></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card ">
                    <div style="margin:0px;" class="card-header row">
                        <div class="col-12">
                            <h4 class="card-title">Activitats</h4>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="col-12 row">
                            <div class="col-3">
                                <i class="nc-icon nc-compass-05 iconBig"></i>
                            </div>
                            <div class="col-9">
                                <span id="activitatsNum" class="numberDisplay"><?php echo count($activitats); ?></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card ">
                    <div style="margin:0px;" class="card-header row">
                        <div class="col-12">
                            <h4 class="card-title">Accions</h4>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="col-12">
                            <a id="editar-0" class="btn btn-info btn-fill col-12 buttonMargin" onclick="desvincularParticipantsModal()" href="#">
                                Desvincular participants
                            </a>
                            <input type="hidden" id="grupId" value="<?php echo $_GET['id'] ?>" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card ">
                    <div style="margin:0px;" class="card-header row">

                        <?php if (isset($resGuardat) && $resGuardat) { ?>
                            <div class="col-md-12">
                                <div class="alert alert-success">
                                    <button type="button" aria-hidden="true" class="close" data-dismiss="alert">
                                        <i class="nc-icon nc-simple-remove"></i>
                                    </button>
                                    <span>
                                        <b> Èxit - </b> S'ha modificat el grup corretament.</span>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="col-12">
                            <h4 class="card-title">Panell d'administració de <strong><?php echo $infoGrup['nom'] ?></strong></h4>
                            <p class="card-category">Selecciona la pestanya que vulguis per navegar per l'activitat.</p>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="col-12">
                            <ul class="nav nav-tabs" id="grupsTab" role="tablist">
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link active" id="home-tab" data-toggle="tab" data-target="#participants" type="button" role="tab" aria-controls="participants" aria-selected="true">Participants</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="profile-tab" data-toggle="tab" data-target="#activitats" type="button" role="tab" aria-controls="activitats" aria-selected="false">Activitats</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="contact-tab" data-toggle="tab" data-target="#administrar" type="button" role="tab" aria-controls="administrar" aria-selected="false">Administrar</button>
                                </li>
                            </ul>
                            <div class="tab-content" id="grupsTabContent">
                                <div class="tab-pane fade show active" id="participants" role="tabpanel" aria-labelledby="participants-tab">
                                    <div class="table-full-width col-12">
                                        <table class="table">
                                            <tbody id="grupPartTable">
                                                <?php $c = 0;
                                                foreach ($participants as $part) { ?>

                                                    <tr>
                                                        <td style="width: 100%"><?php echo $part['nom'] . " " . $part['cognoms'] ?></td>
                                                        <td class="td-actions text-right">
                                                            &nbsp;
                                                            <a id="treure-<?php echo $c ?>" class="btn btn-danger btn-fill" onClick="treureParticipantModal(<?php echo $part['id'] ?>, <?php echo $_GET['id'] ?>)" href="#">
                                                                Treure
                                                            </a>
                                                        </td>
                                                    </tr>
                                                <?php $c++;
                                                } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="activitats" role="tabpanel" aria-labelledby="activitats-tab">
                                    <div class="table-full-width col-12">
                                        <table class="table">
                                            <tbody id="grupActTable">
                                                <?php $c = 0;
                                                foreach ($activitats as $act) { ?>

                                                    <tr>
                                                        <td style="width: 100%"><?php echo $act['nom'] ?></td>
                                                        <td class="td-actions text-right">
                                                            &nbsp;
                                                            <a id="treure-<?php echo $c ?>" class="btn btn-danger btn-fill" onClick="treureActivitatModal(<?php echo $act['id'] ?>, <?php echo $_GET['id'] ?>)" href="#">
                                                                Treure
                                                            </a>
                                                        </td>
                                                    </tr>
                                                <?php $c++;
                                                } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="administrar" role="tabpanel" aria-labelledby="administrar-tab">
                                    <form method="POST" target="_self">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <label>Nom del grup</label>
                                                <input id="nom" name="nom" type="text" class="form-control" placeholder="Nom del grup..." value="<?php echo $infoGrup['nom'] ?>">
                                                <label>Ordre d'aparició</label>
                                                <input id="ordre" name="ordre" type="text" class="form-control" placeholder="Nom del grup..." value="<?php echo $infoGrup['ordre'] ?>">
                                                <label>Visible</label>
                                                <select name="visible" id="visible" class="form-control">
                                                    <option <?php if ($infoGrup['visible'] == 1) echo "selected"; ?> value="1">Si</option>
                                                    <option <?php if ($infoGrup['visible'] == 0) echo "selected"; ?> value="0">No</option>
                                                </select>
                                                <br />
                                                <input class="form-control" type="submit" value="Guardar" />
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer ">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php require_once('footer.php') ?>