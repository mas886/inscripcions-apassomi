<?php
require_once("../../config.php");
require_once("../../controller/functions.php");
generateConnection();

session_start();

if (!isset($_SESSION['user']) || !isset($_SESSION['password']) || !isset($_SESSION['expire_session']) || !($_SESSION['expire_session'] > time()) || !checkAdminLogin($_SESSION['user'], $_SESSION['password'])) {
    exit;
} else {
    if (isset($_POST['action']) && $_POST['action'] == "grupData" && isset($_POST['grupId'])) {
        $query = "SELECT * FROM `grup` WHERE `id`=:id";
        $grups = getQuery($query, [':id' => $_POST['grupId']]);
        echo json_encode($grups);
    } elseif (isset($_POST['action']) && $_POST['action'] == "grupDelete" && isset($_POST['grupId'])) {
        $query = "DELETE FROM `grup` WHERE `id`=:grupId";
        $grups = executeQuery($query, [':grupId' => $_POST['grupId']]);
        if ($grups) {
            echo json_encode(['ERR' => 0, 'MSG' => "Borrats"]);
        } else {
            echo json_encode(['ERR' => 1, 'MSG' => "Hi ha hagut un problema al borrar el grup."]);
        }
    } elseif (isset($_POST['action']) && $_POST['action'] == "getTableParticipants" && isset($_POST['pageWanted']) && isset($_POST['regAmount']) && isset($_POST['ordre']) && isset($_POST['busca'])) {

        $baseQuery = "* FROM `infant`";
        $paramsArray = [':offset' => ($_POST['pageWanted']) * $_POST['regAmount'], ':regAmount' => $_POST['regAmount']];

        $filterParams = [];
        $filterQuery = "";

        if ($_POST['busca'] != "") {
            $aBuscar= '%' . $_POST['busca'] . '%';
            $filterQuery = " WHERE CONCAT(`nom`, ' ', `cognoms`) LIKE :buscar1";
            $baseQuery .= $filterQuery;
            $filterParams = [':buscar1' => $aBuscar];
            $paramsArray = array_merge($paramsArray, $filterParams);
        }

        if ($_POST['ordre'] != "") {
            if ($_POST['ordre'] == 'nom') {
                $baseQuery .= " ORDER BY nom, cognoms";
            } elseif ($_POST['ordre'] == 'cognoms') {
                $baseQuery .= " ORDER BY cognoms, nom";
            } elseif ($_POST['ordre'] == 'data_naix') {
                $baseQuery .= " ORDER BY data_naix DESC, nom";
            }
        }

        $query = "SELECT " . $baseQuery . " LIMIT :offset, :regAmount";

        $participants = getQuery($query, $paramsArray);
        $tableData = "";
        foreach ($participants as $part) {
            $tableData .= "<tr>";
            $tableData .= "<td style=\"width:100%\">" . $part['id'] . " - " . $part['nom'] . " " . $part['cognoms'] . "</td>";
            $tableData .= "<td class=\"td-actions text-right\">";
            $tableData .= "<a class=\"btn btn-warning btn-fill\" href=\"./participantsEditar.php?id=" . $part['id'] . "\">Editar</a>";
            $tableData .= "</td>";
            $tableData .= "</tr>";
        }

        //we get amount of pages and update the buttons
        $query = "SELECT COUNT(`id`) `total` FROM `infant`" . $filterQuery;
        $total = getQuery($query, $filterParams)[0]['total'];

        $pagination = "";
        for ($i = 0; $i < $total / $_POST['regAmount']; $i++) {
            $pagination .= "<button class=\"botPag-$i buttonPaginationItem\" onclick=\"getPageParticipants($i)\">" . ($i + 1) . "</button> ";
        }

        if ($participants) {
            echo json_encode(['ERR' => 0, 'MSG' => "Exit", 'CONT' => $tableData, 'PAGS' => $pagination, 'TOT' => $total]);
        } else {
            echo json_encode(['ERR' => 1, 'MSG' => "No s'ha trobat participants."]);
        }
    } elseif (isset($_POST['action']) && $_POST['action'] == "getGrups") {
        $query = "SELECT * FROM `grup` WHERE `visible`=1 ORDER BY `ordre` ASC";
        $grups = getQuery($query);
        if ($grups) {
            echo json_encode(['ERR' => 0, 'MSG' => $grups]);
        } else {
            echo json_encode(['ERR' => 1, 'MSG' => "Hi ha hagut un problema en recuperar les activitats. Assegura't que n'hi ha i estan visibles."]);
        }
    } elseif (isset($_POST['action']) && $_POST['action'] == "carregarParticipants" && isset($_POST['participants']) && isset($_POST['grupId'])) {

        include_once('../../model/modelInfant.php');

        //Borra les entrades més antigues de 30 dies.
        Infant::deleteProvOld();

        $thingIs = explode("\n", $_POST['participants']);

        if(sizeof($thingIs)>2){
            unset($thingIs[0]);
    
            $infant = new Infant;
    
            $remesaId = time();
    
            foreach ($thingIs as $thing) {
                $explodedThing = explode(";", $thing);
                if (isset($explodedThing[0]) and  isset($explodedThing[1]) and isset($explodedThing[2]) and isset($explodedThing[3]) and isset($explodedThing[4]) and isset($explodedThing[5]) and isset($explodedThing[6])) {
                    $explodedThing[2] = implode("-", array_reverse(explode("/", explode(" ", $explodedThing[2])[0])));
                    $infant->addIntoDbProv($explodedThing[0], $explodedThing[1], $explodedThing[2], $explodedThing[3], $explodedThing[4], $explodedThing[5], $explodedThing[6], $remesaId, $_POST['grupId']);
                }
            }
            echo json_encode(['ERR' => 0, 'MSG' => $remesaId]);
        }else{
            echo json_encode(['ERR' => 1, 'MSG' => "L'arxiu és incorrecte o no està ben formatat."]);
        }
    } elseif (isset($_POST['action']) && $_POST['action'] == "validarParticipants" && isset($_POST['remesaId'])) {

        include_once('../../model/modelInfant.php');

        $infant = new Infant;

        $query = "SELECT * FROM `infant_provisional` WHERE `remesaId`=:remesaId";
        $participantsPrev = getQuery($query, [':remesaId' => $_POST['remesaId']]);

        foreach ($participantsPrev as $part) {
            $partId = $infant->addIntoDb($part['nom'], $part['cognoms'], $part['data_naix'], $part['telefon_altre'], $part['telefon_fix'], $part['telefon_mare'], $part['telefon_pare']);
            Infant::assignGrup($partId, $part['grupId']);
        }
        echo json_encode(['ERR' => 0, 'MSG' => $part['grupId']]);
    } elseif (isset($_POST['action']) && $_POST['action'] == "treureParticipant" && isset($_POST['participantId']) && isset($_POST['grupId'])) {

        include_once('../../model/modelInfant.php');

        if (Infant::unassignGrup($_POST['participantId'], $_POST['grupId'])) {
            $html = "";

            $query = "SELECT `id`, `nom`, `cognoms` FROM `infant_grup` ig JOIN `infant` i ON i.id = ig.infant_id WHERE grup_id = :idGrup ORDER BY nom";
            $participants = getQuery($query, [':idGrup' => $_POST['grupId']]);
            $c = 0;
            foreach ($participants as $part) {
                $html .= "<tr>";
                $html .= "<td style=\"width: 100%\">" . $part['nom'] . " " . $part['cognoms'] . "</td>";
                $html .= "<td class=\"td-actions text-right\">";
                $html .= "&nbsp;<a id=\"treure-" . $c . "\" class=\"btn btn-danger btn-fill\" onClick=\"treureParticipantModal(" . $part['id'] . "," . $_POST['grupId'] . ")\" href=\"#\">Treure</a>";
                $html .= "</td>";
                $html .= "</tr>";
                $c++;
            }

            echo json_encode(['ERR' => 0, 'MSG' => $html]);
        } else {
            echo json_encode(['ERR' => 1, 'MSG' => "Error"]);
        }
    } elseif (isset($_POST['action']) && $_POST['action'] == "treureActivitat" && isset($_POST['activitatId']) && isset($_POST['grupId'])) {

        $query = "DELETE FROM `activitat_grup` WHERE `grup_id` = :grupId AND `activitat_id` = :activitatId";

        if (executeQuery($query, [':grupId' => $_POST['grupId'], ':activitatId' => $_POST['activitatId']])) {
            $html = "";

            $query = "SELECT * FROM `activitat_grup` JOIN `activitat` a ON a.id = `activitat_id` WHERE grup_id = :grupId ORDER BY a.id DESC";
            $activitats = getQuery($query, [':grupId' => $_POST['grupId']]);

            $c = 0;
            foreach ($activitats as $act) {
                $html .= "<tr>";
                $html .= "<td style=\"width: 100%\">" . $act['nom'] . "</td>";
                $html .= "<td class=\"td-actions text-right\">";
                $html .= "&nbsp;<a id=\"treure-$c\" class=\"btn btn-danger btn-fill\" onClick=\"treureActivitatModal(" . $act['id'] . "," . $_POST['grupId'] . ")\" href=\"#\">Treure</a>";
                $html .= "</td>";
                $html .= "</tr>";
                $c++;
            }

            echo json_encode(['ERR' => 0, 'MSG' => $html]);
        } else {
            echo json_encode(['ERR' => 1, 'MSG' => "Error"]);
        }
    } elseif (isset($_POST['action']) && $_POST['action'] == "desvincularParticipants" && isset($_POST['grupId'])) {

        $query = "DELETE FROM `infant_grup` WHERE `grup_id` = :grupId";

        if (executeQuery($query, [':grupId' => $_POST['grupId']])) {
            echo json_encode(['ERR' => 0, 'MSG' => ""]);
        } else {
            echo json_encode(['ERR' => 1, 'MSG' => "Error"]);
        }
    } elseif (isset($_POST['action']) && $_POST['action'] == "desvincularTots") {

        $query = "DELETE FROM `infant_grup` WHERE 1";

        if (executeQuery($query)) {
            echo json_encode(['ERR' => 0, 'MSG' => ""]);
        } else {
            echo json_encode(['ERR' => 1, 'MSG' => "Error"]);
        }
    } elseif (isset($_POST['action']) && $_POST['action'] == "esborrarActivitat" && isset($_POST['activitatId'])) {
        
        $query = "DELETE FROM `activitat` WHERE `id`=:activitatId";
        
        if (executeQuery($query, [':activitatId' => $_POST['activitatId']])) {
            echo json_encode(['ERR' => 0, 'MSG' => ""]);
        } else {
            echo json_encode(['ERR' => 1, 'MSG' => "Hi ha hagut un problema al borrar l'activitat. Assegura't primer de que no tingui participants inscrits o grups vinculats."]);
        }
    }
}
