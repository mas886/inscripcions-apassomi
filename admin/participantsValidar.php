<?php require_once("header.php");

$query = "SELECT * FROM `infant_provisional` WHERE `remesaId` = :remesaId";
$participantsProv = getQuery($query, [':remesaId' => $_GET['batchId']]);

if(count($participantsProv)==0){
    echo ("<script>window.location.href = \"./index.php\";</script>");
    exit;
}else{
    $query = "SELECT DISTINCT `grupId`, grup.`nom` nomGrup FROM `infant_provisional` JOIN grup ON `grup`.id = `grupId` WHERE `remesaId`=:remesaId";
    $grup = getQuery($query, [':remesaId' => $_GET['batchId']])[0];
}


?>

<!-- End Navbar -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card ">
                    <div style="margin:0px;" class="card-header row">
                        <div class="col-10">
                            <h4 class="card-title">Validar lot de participants</h4>
                            <p class="card-category">Acabes de carregar un nou lot de participants. Assegura't que el format de les dates entrades és el correcte. Si veus coses estranyes <strong>NO</strong> ho validis i revisa el document entrat.</p>
                        </div>
                        <div class="col-2 text-right" style="padding-right: 0px;">
                            <a class="btn btn-success btn-fill" onClick="validarParticipantsLotModal()" href="#">
                                Validar valors
                            </a>
                            <input type="hidden" id="batchId" name="batchId" value="<?php echo $_GET['batchId'] ?>" />
                            <input type="hidden" id="grupId" name="grupId" value="<?php echo $_GET['batchId'] ?>" />
                        </div>
                    </div>
                    <div class="card-body ">
                        <div clas="row">
                            <div class="col-12"><strong>Grup:</strong> <?php echo $grup['nomGrup'] ?><br /><br /></div>
                        </div>
                        <div clas="row">
                            <div class="col-12">
                                <table class="table table-hover table-striped">
                                    <thead>
                                        <tr>
                                            <th>Nom</th>
                                            <th>Cognoms</th>
                                            <th>Data Naixement</th>
                                            <th>Telèfon Altre</th>
                                            <th>Telèfon Fix</th>
                                            <th>Telèfon Mare</th>
                                            <th>Telèfon Pare</th>
                                            <th>Acció</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $c = 0;
                                        foreach ($participantsProv as $participant) { ?>
                                            <tr>
                                                <td><?php echo $participant['nom'] ?></td>
                                                <td><?php echo $participant['cognoms'] ?></td>
                                                <td><?php echo $participant['data_naix'] ?></td>
                                                <td><?php echo $participant['telefon_altre'] ?></td>
                                                <td><?php echo $participant['telefon_fix'] ?></td>
                                                <td><?php echo $participant['telefon_mare'] ?></td>
                                                <td><?php echo $participant['telefon_pare'] ?></td>
                                                <td><?php echo $participant['action'] ?></td>
                                            </tr>
                                        <?php $c++;
                                        } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer ">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php require_once('footer.php') ?>