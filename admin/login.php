<?php

session_start();
        
require_once("../config.php");
require_once("../controller/functions.php");

if(isset($_SESSION['user']) && isset($_SESSION['password']) && isset($_SESSION['expire_session'])){
    if(checkAdminLogin($_SESSION['user'],$_SESSION['password']) && ($_SESSION['expire_session']>time())){
        header("Location: ./");
    }else{
        unset($_SESSION['user']);
        unset($_SESSION['password']);
        unset($_SESSION['expire_session']);
    }
}

if(isset($_POST['user']) && isset($_POST['password'])){
    if(checkAdminLogin($_POST['user'],$_POST['password'])){
        $_SESSION['expire_session']=time() + ( 60 * 60);
        $_SESSION['user']=$_POST['user'];
        $_SESSION['password']=$_POST['password'];
        header("Location: ./");
    }
}

?>

<html>
  <head>
  
  <style>
    body#LoginForm{ background-color:#61ad70}

    .form-heading { color:#fff; font-size:23px;}
    .panel h2{ color:#444444; font-size:18px; margin:0 0 8px 0;}
    .panel p { color:#777777; font-size:14px; margin-bottom:30px; line-height:24px;}
    .login-form .form-control {
    background: #f7f7f7 none repeat scroll 0 0;
    border: 1px solid #d4d4d4;
    border-radius: 4px;
    font-size: 14px;
    height: 50px;
    line-height: 50px;
    }
    .main-div {
    background: #ffffff none repeat scroll 0 0;
    border-radius: 2px;
    margin: 10px auto 30px;
    max-width: 500px;
    width: 90%;
    padding: 50px 70px 70px 71px;
    }

    .login-form .form-group {
    margin-bottom:10px;
    }
    .login-form{ text-align:center;}
    .forgot a {
    color: #777777;
    font-size: 14px;
    text-decoration: underline;
    }
    .login-form  .btn.btn-primary {
    background: #61ad70 none repeat scroll 0 0;
    border-color: #61ad70;
    color: #ffffff;
    font-size: 14px;
    width: 100%;
    height: 50px;
    line-height: 50px;
    padding: 0;
    }
    .forgot {
    text-align: left; margin-bottom:30px;
    }
    .botto-text {
    color: #ffffff;
    font-size: 14px;
    margin: auto;
    }
    .login-form .btn.btn-primary.reset {
    background: #ff9900 none repeat scroll 0 0;
    }
    .back { text-align: left; margin-top:10px;}
    .back a {color: #444444; font-size: 13px;text-decoration: none;}

  </style>
<link href="./assets/css/bootstrap.min.css" rel="stylesheet" />
    <meta name="robots" content="noindex">
    <title>Login - Admin inscripcions Grup d'Esplai Apassomi</title>
  </head>
<body id="LoginForm">
<div class="container">
<br/><br/><br/>
<div class="login-form">
<div class="main-div">
    <div class="panel">
   <h2>Admin Login</h2>
   <p>Escriu l'usuari i la contrassenya</p>
   </div>
    <form method="POST" target="_self">

        <div class="form-group">


            <input type="text" class="form-control" name="user" placeholder="Usuari">

        </div>

        <div class="form-group">

            <input type="password" class="form-control" name="password" placeholder="Contrasenya">

        </div>
        <button type="submit" class="btn btn-primary">Entra</button>

    </form>
    </div>
</div></div></div>


</body>
</html>
 
