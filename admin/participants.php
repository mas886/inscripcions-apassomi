<?php require_once("header.php");

$defRegistres = 25;

$query = "SELECT * FROM `infant` LIMIT :offset,:registres";
$participants = getQuery($query, [':offset' => (int) 0, ':registres' => $defRegistres]);

$query = "SELECT COUNT(`id`) `total` FROM `infant` WHERE 1";
$total = getQuery($query, [])[0]['total'];

?>

<!-- End Navbar -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4">
                <div class="card ">
                    <div style="margin:0px;" class="card-header row">
                        <div class="col-12">
                            <h4 class="card-title">Participants</h4>
                            <p class="card-category">Participants totals registrats.</p>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="col-12 row">
                            <div class="col-3">
                                <i class="nc-icon nc-single-02 iconBig"></i>
                            </div>
                            <div class="col-9">
                                <span class="numberDisplay"><?php echo $total ?></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card ">
                    <div style="margin:0px;" class="card-header row">
                        <div class="col-12">
                            <h4 class="card-title">Accions</h4>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="col-12">
                            <a id="editar-0" class="btn btn-info btn-fill col-12 buttonMargin" onclick="carregarParticipantsModal()" href="#">
                                Carregar participants
                            </a>
                            <a id="editar-0" class="btn btn-danger col-12 buttonMargin" onclick="desvincularTotsModal()" href="#">
                                Desvincular activitats
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card ">
                    <div style="margin:0px;" class="card-header row">

                        <?php if (isset($resGuardat) && $resGuardat) { ?>
                            <div class="col-md-12">
                                <div class="alert alert-success">
                                    <button type="button" aria-hidden="true" class="close" data-dismiss="alert">
                                        <i class="nc-icon nc-simple-remove"></i>
                                    </button>
                                    <span>
                                        <b>Èxit - </b> S'ha modificat el grup corretament.</span>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="col-12">
                            <h4 class="card-title">Participants</h4>
                            <p class="card-category">Aquesta llista mostra els participants registrats a la plataforma, vinculats o no a alguna activitat.</p>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="col-12 row">
                            <div class="col-md-6">
                                <label>Busca participant</label>
                                <input id="partSearch" type="text" class=" form-control" placeholder="Introdueix nom o cognoms...">
                            </div>
                            <div class="col-md-2">
                                <label>Nº regs.</label>
                                <select id="partAmount" class="form-control">
                                    <option value="25">25</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                    <option value="250">250</option>
                                    <option value="500">500</option>
                                </select>
                            </div>
                            <div class="col-md-2">
                                <label>Ordena</label>
                                <select  id="partOrd" class="form-control">
                                    <option value="">Defecte</option>
                                    <option value="data_naix">Edat</option>
                                    <option value="nom">Nom</option>
                                    <option value="cognoms">Cognom</option>
                                </select>
                            </div>
                            <div class="col-md-2">
                                <label>&nbsp;</label>
                                <button onClick="filtrarParticipants()" class="form-control">Filtrar</button>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="table-full-width">
                                <table class="table">
                                    <tbody id="participantsTable">
                                        <?php
                                        foreach ($participants as $part) { ?>

                                            <tr>
                                                <td style="width:100%"><?php echo $part['id'] . " - " . $part['nom'] . " " . $part['cognoms']; ?></td>
                                                <td class="td-actions text-right">
                                                    <a id="editar" class="btn btn-warning btn-fill" onClick="" href="./participantsEditar.php?id=<?php echo $part['id']; ?>">
                                                        Editar
                                                    </a>
                                                </td>
                                            </tr>

                                        <?php
                                        } ?>
                                    </tbody>
                                </table>
                                <div class="tableLoadingDiv" style="display: none;"><span class="loadingTableText">Carregant contingut...</span></div>
                                <div class="col-12">
                                    <div class="row">
                                        <div class="col-3">
                                            <label><?php echo "Mostrant <span id='amountRegs'>$defRegistres</span> de <span id='totalRegs'>$total</span> registres"; ?></label>
                                            <input id="defRegistres" type="hidden" value="<?php echo $defRegistres; ?>"/>
                                            <input id="defOrdenacio" type="hidden" value=""/>
                                            <input id="defBusca" type="hidden" value=""/>
                                            <input id="curPage" type="hidden" value="0"/>
                                        </div>
                                        <div class="col-2">
                                            <button class="buttonPagination buttonPagPrev" onclick="getPrevParticipants()" disabled>➔</button>
                                        </div>
                                        <div id="paginationButtons" class="col-5 text-center">
                                            <?php for ($i = 0; $i < $total / $defRegistres; $i++) { ?>
                                                <button <?php if($i==0) echo "disabled" ?> class="botPag-<?php echo $i." ";  if($i==0) echo "actual" ?> buttonPaginationItem" onclick="getPageParticipants(<?php echo $i ?>)"><?php echo $i+1 ?></button>
                                            <?php } ?>
                                        </div>
                                        <div class="col-2 text-right">
                                            <button class="buttonPagination buttonPagNext" onclick="getNextParticipants()">➔</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer ">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php require_once('footer.php') ?>