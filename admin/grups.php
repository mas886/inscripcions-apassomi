<?php require_once("header.php");

if (isset($_POST['nomGrup']) && isset($_POST['ordreGrup']) && isset($_POST['visibleGrup'])) {
    if (!isset($_POST['idGrup'])) {
        $query = "INSERT INTO `grup`(`nom`, `ordre`, `visible`) VALUES (:nom,:ordre,:visible)";
        $activitats = executeQuery($query, [':nom' => $_POST['nomGrup'], ':ordre' => $_POST['ordreGrup'], ':visible' => $_POST['visibleGrup']]);
    } else {
        $query = "UPDATE `grup` SET `nom`=:nom ,`ordre`=:ordre ,`visible`=:visible WHERE `id`= :idGrup";
        $activitats = executeQuery($query, [':nom' => $_POST['nomGrup'], ':ordre' => $_POST['ordreGrup'], ':visible' => $_POST['visibleGrup'], ':idGrup' => $_POST['idGrup']]);
    }
}

$query = "SELECT * FROM `grup` WHERE `visible`=1 ORDER BY `ordre` ASC";
$grupsActius = getQuery($query, []);

$query = "SELECT * FROM `grup` WHERE `visible`=0 ORDER BY `ordre` ASC";
$grupsInactius = getQuery($query, []);
?>

<!-- End Navbar -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card ">
                    <div style="margin:0px;" class="card-header row">
                        <div class="col-8">
                            <h4 class="card-title">Grups</h4>
                            <p class="card-category">Llistat de grups. Cada infant i cada activitat s'assigna a algún grup.</p>
                        </div>
                        <div class="col-4 text-right" style="padding-right: 0px;">
                            <a class="btn btn-success btn-fill" onClick="crearGrupModal()" href="#">
                                Crear
                            </a>
                        </div>
                    </div>
                    <div class="card-body ">
                        <div class="table-full-width">
                            <table class="table">
                                <tbody>
                                    <?php $c = 0;
                                    foreach ($grupsActius as $grup) { ?>

                                        <tr>
                                            <td style="width: 100%"><?php echo $grup['nom'] ?> - #<?php echo $grup['ordre'] ?></td>
                                            <td class="td-actions text-right">
                                                <a class="btn btn-info btn-fill" href="./grupsDetall.php?id=<?php echo $grup['id']; ?>">
                                                    Veure
                                                </a>
                                                &nbsp;
                                                <a id="editar-<?php echo $c ?>" class="btn btn-warning btn-fill" onClick="editarGrupModal(<?php echo $grup['id']; ?>, 'editar-<?php echo $c ?>')" href="#">
                                                    Editar
                                                </a>
                                                &nbsp;
                                                <a id="borrar-<?php echo $c ?>" class="btn btn-danger btn-fill" onClick="borrarGrupModal(<?php echo $grup['id']; ?>, 'borrar-<?php echo $c ?>')" href="#">
                                                    Esborrar
                                                </a>
                                            </td>
                                        </tr>

                                    <?php $c++;
                                    } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card-footer ">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card ">
                    <div style="margin:0px;" class="card-header row">
                        <div class="col-12">
                            <h4 class="card-title">Grups Invisibles</h4>
                        </div>
                    </div>
                    <div class="card-body ">
                        <div class="table-full-width">
                            <table class="table">
                                <tbody>
                                    <?php $c = 0;
                                    foreach ($grupsInactius as $grup) { ?>

                                        <tr>
                                            <td style="width: 100%"><?php echo $grup['nom'] ?> - #<?php echo $grup['ordre'] ?></td>
                                            <td class="td-actions text-right">
                                                <a class="btn btn-info btn-fill" href="./grupsDetall.php?id=<?php echo $grup['id']; ?>">
                                                    Veure
                                                </a>
                                                &nbsp;
                                                <a id="editar-<?php echo $c ?>" class="btn btn-warning btn-fill" onClick="editarGrupModal(<?php echo $grup['id']; ?>, 'editar-<?php echo $c ?>')" href="#">
                                                    Editar
                                                </a>
                                                &nbsp;
                                                <a id="borrar-<?php echo $c ?>" class="btn btn-danger btn-fill" onClick="borrarGrupModal(<?php echo $grup['id']; ?>, 'borrar-<?php echo $c ?>')" href="#">
                                                    Esborrar
                                                </a>
                                            </td>
                                        </tr>

                                    <?php $c++;
                                    } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card-footer ">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php require_once('footer.php') ?>