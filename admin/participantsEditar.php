<?php require_once("header.php");

$query = "SELECT * FROM `grup` WHERE `visible`=1 ORDER BY `ordre` ASC";
$grupsActius = getQuery($query, []);

if (isset($_POST['nom']) && isset($_POST['cognoms']) && isset($_POST['dataNaix']) && isset($_POST['telefonAltre']) && isset($_POST['telefonFix']) && isset($_POST['telefonMare']) && isset($_POST['telefonPare']) && isset($_POST['id'])) {
    $query = "UPDATE `infant` SET `nom`=:nom,`cognoms`=:cognoms,`data_naix`=:dataNaix,`telefon_altre`=:telefonAltre,`telefon_fix`=:telefonFix,`telefon_mare`=:telefonMare,`telefon_pare`=:telefonPare WHERE `id`=:id";
    $resGuardat = executeQuery($query, [':nom' => $_POST['nom'], ':cognoms' => $_POST['cognoms'], ':dataNaix' => $_POST['dataNaix'], ':telefonAltre' => $_POST['telefonAltre'], ':telefonFix' => $_POST['telefonFix'], ':telefonMare' => $_POST['telefonMare'], ':telefonPare' => $_POST['telefonPare'], ':id' => $_POST['id']]);
    
    if ($resGuardat) {
        executeQuery("DELETE FROM `infant_grup` WHERE `infant_id` = :infantId", [':infantId' => $_POST['id']]);
        foreach($grupsActius as $grup){
            if(isset($_POST["grup-".$grup['id']]) && $_POST["grup-".$grup['id']]==1){
                $query="INSERT INTO `infant_grup`(`infant_id`, `grup_id`) VALUES (:infantId, :grupId)";
                executeQuery($query, [':infantId' => $_POST['id'], ':grupId' => $grup['id']]);
            }
        }
        $works = "!";
    }
}

if (isset($_GET['id']) && $_GET['id'] != '' && is_numeric($_GET['id'])) {

    $query = "SELECT * FROM `infant` WHERE id=:id";
    $infant = getQuery($query, [":id" => $_GET['id']]);
    if (!$infant) {
        echo ("<script>window.location.href = \"./participants.php\";</script>");
        exit;
    } else {
        $infant = $infant[0];
    }
} else {
    echo ("<script>window.location.href = \"./participants.php\";</script>");
    exit;
}

$query = "SELECT `grup_id` FROM `infant_grup` WHERE `infant_id`=:infantId";
$grupsActivitat = getQuery($query, [':infantId'=>$_GET['id']]);

$grupsSelected =[];
foreach($grupsActivitat as $gt){
    $grupsSelected[]=$gt['grup_id'];
}

?>

<!-- End Navbar -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card ">
                    <div style="margin:0px;" class="card-header row">
                        <div class="col-8">
                            <h4 class="card-title">Editar Participant: <?php echo $infant['nom'] . " " . $infant['cognoms'] ?></h4>
                            <p class="card-category">Estàs editant el participant, recorda <b>guardar</b> els canvis</p>
                        </div>
                        <div class="col-4 text-right" style="padding-right: 0px;">
                            <a class="icon-big" href="./participants.php"><i class="fas fa-arrow-left"></i></a>
                        </div>
                    </div>
                    <div class="card-body ">
                        <?php if (isset($resGuardat) && $resGuardat) { ?>
                            <div class="col-md-12">
                                <div class="alert alert-success">
                                    <button type="button" aria-hidden="true" class="close" data-dismiss="alert">
                                        <i class="nc-icon nc-simple-remove"></i>
                                    </button>
                                    <span>
                                        <b> Èxit - </b> S'ha guardat el participant correctament.</span>
                                </div>
                            </div>
                        <?php } else if (isset($resGuardat) && !$resGuardat) { ?>
                            <div class="col-md-12">
                                <div class="alert alert-danger">
                                    <button type="button" aria-hidden="true" class="close" data-dismiss="alert">
                                        <i class="nc-icon nc-simple-remove"></i>
                                    </button>
                                    <span>
                                        <b> Error! - </b> Hi ha hagut algún tipus de problema...</span>
                                </div>
                            </div>
                        <?php } ?>
                        <div style="margin:0px;" class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <span>Data d'alta del participant: <?php echo $infant['data_alta'] ?></span>
                                </div>
                            </div>
                        </div>
                        <form method="POST" target="_self">
                            <div style="margin:0px;" class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Nom</label>
                                        <input required class="form-control" name="nom" placeholder="Nom participant..." type="text" value="<?php echo $infant['nom'] ?>">
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label>Cognoms</label>
                                        <input required class="form-control" name="cognoms" placeholder="Cognoms participant..." type="text" value="<?php echo $infant['cognoms'] ?>">
                                    </div>
                                </div>
                            </div>
                            <div style="margin:0px;" class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Data Naixement</label>
                                        <input required id="dataNaix" class="form-control" name="dataNaix" placeholder="Data de naixement..." type="text" value="<?php echo substr($infant['data_naix'], 0, 10) ?>">
                                    </div>
                                </div>
                            </div>
                            <div style="margin:0px;" class="row">
                                <div class="col-md-6 pr-1">
                                    <div class="form-group">
                                        <label>Telèfon Altre</label>
                                        <input class="form-control" placeholder="Telefon altre..." name="telefonAltre" type="text" value="<?php echo $infant['telefon_altre'] ?>">
                                    </div>
                                </div>
                                <div class="col-md-6 pr-1">
                                    <div class="form-group">
                                        <label>Telèfon Fix</label>
                                        <input class="form-control" placeholder="Telefon fix..." name="telefonFix" type="text" value="<?php echo $infant['telefon_fix'] ?>">
                                    </div>
                                </div>
                            </div>
                            <div style="margin:0px;" class="row">
                                <div class="col-md-6 pr-1">
                                    <div class="form-group">
                                        <label>Telèfon Mare</label>
                                        <input class="form-control" placeholder="Telefon mare..." name="telefonMare" type="text" value="<?php echo $infant['telefon_mare'] ?>">
                                    </div>
                                </div>
                                <div class="col-md-6 pr-1">
                                    <div class="form-group">
                                        <label>Telèfon Pare</label>
                                        <input class="form-control" placeholder="Telefon pare..." name="telefonPare" type="text" value="<?php echo $infant['telefon_pare'] ?>">
                                    </div>
                                </div>
                            </div>
                            
                            <div style="margin:0px;" class="row">
                                <div class="col-md-12">
                                    <h4 class="card-title">Grups</h4>
                                    <p class="card-category">Selecciona, fent clic, els grups dels que forma part el participant. Se'n pot seleccionar més d'un.</p>
                                </div>
                            </div>
                            <div style="margin:0px;" class="row">
                                <?php foreach ($grupsActius as $grup) { ?>
                                    <div class="col-12 col-sm-6 col-md-3 grupSelectorHolder">
                                        <div id="grupSel-<?php echo $grup['id'] ?>" class="grupSelector <?php if(in_array($grup['id'],$grupsSelected)) echo "selected"; ?>" onclick="selectGrupCrear(<?php echo $grup['id'] ?>)">
                                            <span><?php echo $grup['nom'] ?></span>
                                            <input name="grup-<?php echo $grup['id'] ?>" id="grup-<?php echo $grup['id'] ?>" type="hidden" value="<?php if(in_array($grup['id'],$grupsSelected)){ echo "1"; }else {echo "0";} ?>">
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                            
                            <input type="hidden" name="id" value="<?php echo $infant['id'] ?>" />
                            <div style="margin:0px;" class="row">
                                <div class="col-3">
                                    <br/>
                                    <div class="form-group">
                                        <input class="form-control" type="submit" value="Guardar">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="card-footer ">
                    </div>
                </div>
                <!-- Mini Modal -->
                <div class="modal fade modal-mini modal-primary" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header justify-content-center">
                                <div class="modal-profile">
                                    <i class="nc-icon nc-ambulance"></i>
                                </div>
                            </div>
                            <div class="modal-body text-center">
                                <h2>Alerta!</h2>
                                <p>Estàs a punt de borrar l'activitat: <b><?php echo $activitat[0]['nom'] ?></b>, n'estàs segur?</p>
                            </div>
                            <div class="modal-footer">
                                <form target="_self" method="POST">
                                    <input type="hidden" name="accio" value="borrar" />
                                    <input type="hidden" name="id" value="<?php echo $activitat[0]['id'] ?>" />
                                    <input type="submit" class="btn btn-link btn-simple" value="Sí">
                                </form>
                                <button type="button" class="btn btn-link btn-simple" data-dismiss="modal">ENRERE</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!--  End Modal -->
            </div>
        </div>
    </div>
</div>
<?php require_once('footer.php') ?>
<?php require_once('./assets/js/formularis.js.php') ?>