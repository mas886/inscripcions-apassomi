<?php require_once("header.php");

$query = "SELECT * FROM `grup` WHERE `visible`=1 ORDER BY `ordre` ASC";
$grupsActius = getQuery($query, []);

if (isset($_POST['accio'])) {
    if ($_POST['accio'] == "editar") {
        if (isset($_POST['nom']) && isset($_POST['data']) && isset($_POST['datainici']) && isset($_POST['datafi']) && isset($_POST['preu']) && isset($_POST['id'])) {
            if ($_POST['nom'] != '' && $_POST['data'] != '' && $_POST['datainici'] != '' && $_POST['datafi'] != '' && $_POST['preu'] != '' && $_POST['id'] != "") {
                if (isset($_POST['visible'])) {
                    $_POST['visible'] = 1;
                } else {
                    $_POST['visible'] = 0;
                }
                $query = "UPDATE `activitat` SET `nom`= :nom , `preu`= :preu , `visible`= :visible ,`data`= :data ,`inici_inscripcions`= :datainici ,`final_inscripcions`= :datafi WHERE `id`= :id";
                $activitats = executeQuery($query, [':nom' => $_POST['nom'], ':preu' => $_POST['preu'], ':visible' => $_POST['visible'], ':data' => $_POST['data'], ':datainici' => $_POST['datainici'], ':datafi' => $_POST['datafi'], ':id' => $_POST['id']]);

                if ($activitats) {
                    executeQuery("DELETE FROM `activitat_grup` WHERE `activitat_id` = :activitatId", [':activitatId' => $_POST['id']]);
                    foreach($grupsActius as $grup){
                        if(isset($_POST["grup-".$grup['id']]) && $_POST["grup-".$grup['id']]==1){
                            $query="INSERT INTO `activitat_grup`(`grup_id`, `activitat_id`) VALUES (:grupId,:activitatId)";
                            executeQuery($query, [':grupId' => $grup['id'], ':activitatId' => $_POST['id']]);
                        }
                    }
                    $works = "!";
                } else {
                    $works = ":(";
                }
            }
        }
    } else if ($_POST['accio'] == "borrar" && isset($_POST['id'])) {
        $query = "DELETE FROM `activitat` WHERE `id`=:id";
        $activitats = executeQuery($query, [':id' => $_POST['id']]);
        if ($activitats) {
            echo ("<script>window.location.href = \"./index.php\";</script>");
            exit;
        } else {
            $works = ":(";
        }
    }
}

if (isset($_GET['id']) && $_GET['id'] != '' && is_numeric($_GET['id'])) {
    $query = "SELECT * FROM `activitat` WHERE id=:id";
    $activitat = getQuery($query, [":id" => $_GET['id']]);
    if (!$activitat) {
        echo ("<script>window.location.href = \"./index.php\";</script>");
        exit;
    }
} else {
    echo ("<script>window.location.href = \"./index.php\";</script>");
    exit;
}

$query = "SELECT `grup_id` FROM `activitat_grup` WHERE `activitat_id`=:activitatId";
$grupsActivitat = getQuery($query, [':activitatId'=>$_GET['id']]);

$grupsSelected =[];
foreach($grupsActivitat as $gt){
    $grupsSelected[]=$gt['grup_id'];
}

?>

<!-- End Navbar -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card ">
                    <div style="margin:0px;" class="card-header row">
                        <div class="col-8">
                            <h4 class="card-title">Editar Activitat</h4>
                            <p class="card-category">Estàs editant l'activitat, recorda <b>guardar</b> els canvis</p>
                        </div>
                        <div class="col-4 text-right" style="padding-right: 0px;">
                            <a class="icon-big" href="#" onclick="esborrarActivitatModal(<?php echo $_GET['id'] ?>)">
                                <i class="trash-can fas fa-trash-alt"></i></a>&nbsp;&nbsp;
                            <a class="icon-big" href="./"><i class="fas fa-arrow-left"></i></a>
                        </div>
                    </div>
                    <div class="card-body ">
                        <?php if (isset($works) && $works == "!") { ?>
                            <div class="col-md-12">
                                <div class="alert alert-success">
                                    <button type="button" aria-hidden="true" class="close" data-dismiss="alert">
                                        <i class="nc-icon nc-simple-remove"></i>
                                    </button>
                                    <span>
                                        <b> Èxit - </b> S'ha guardat l'activitat correctament.</span>
                                </div>
                            </div>
                        <?php } else if (isset($works) && $works == ":(") { ?>
                            <div class="col-md-12">
                                <div class="alert alert-danger">
                                    <button type="button" aria-hidden="true" class="close" data-dismiss="alert">
                                        <i class="nc-icon nc-simple-remove"></i>
                                    </button>
                                    <span>
                                        <b> Error! - </b> Hi ha hagut algún tipus de problema, si no es possible eliminar l'activitat, fes-la invisible.</span>
                                </div>
                            </div>
                        <?php } ?>
                        <form method="POST" target="_self">
                            <div style="margin:0px;" class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Nom Activitat</label>
                                        <input required class="form-control" name="nom" placeholder="Nom activitat..." type="text" value="<?php echo $activitat[0]['nom'] ?>">
                                    </div>
                                </div>
                            </div>
                            <div style="margin:0px;" class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Data Activitat</label>
                                        <input required id="data-activitat" class="form-control" name="data" placeholder="Data de realitzacio.." type="text" value="<?php echo substr($activitat[0]['data'], 0, 10) ?>">
                                    </div>
                                </div>
                            </div>
                            <div style="margin:0px;" class="row">
                                <div class="col-md-6 pr-1">
                                    <div class="form-group">
                                        <label>Inici d'inscripcions</label>
                                        <input required id="inici-inscripcions" class="form-control" placeholder="Data d'inici de les inscripcions" name="datainici" type="text" value="<?php echo substr($activitat[0]['inici_inscripcions'], 0, 10) ?>">
                                    </div>
                                </div>
                                <div class="col-md-6 pl-1">
                                    <div class="form-group">
                                        <label>Fi d'inscripicons</label>
                                        <input required id="fi-inscripcions" class="form-control" placeholder="Data de fi de les inscripcions" name="datafi" type="text" value="<?php echo substr($activitat[0]['final_inscripcions'], 0, 10) ?>">
                                    </div>
                                </div>
                            </div>
                            <div style="margin:0px;" class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Preu</label>
                                        <input required class="form-control" name="preu" placeholder="Preu de l'activitat..." step=".01" type="number" value="<?php echo $activitat[0]['preu'] ?>">
                                    </div>
                                </div>
                            </div>
                            <div style="margin:0px;" class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Es visible</label>
                                        <input name="visible" style="vertical-align: middle;" type="checkbox" <?php if ($activitat[0]['visible'] == 1) echo "checked"; ?> />
                                    </div>
                                </div>
                            </div>
                            <div style="margin:0px;" class="row">
                                <div class="col-md-12">
                                    <h4 class="card-title">Grups</h4>
                                    <p class="card-category">Selecciona, fent clic, els grups on l'activitat estarà disponible. Es pot seleccionar més d'un grup.</p>
                                </div>
                            </div>
                            <div style="margin:0px;" class="row">
                                <?php foreach ($grupsActius as $grup) { ?>
                                    <div class="col-12 col-sm-6 col-md-3 grupSelectorHolder">
                                        <div id="grupSel-<?php echo $grup['id'] ?>" class="grupSelector <?php if(in_array($grup['id'],$grupsSelected)) echo "selected"; ?>" onclick="selectGrupCrear(<?php echo $grup['id'] ?>)">
                                            <span><?php echo $grup['nom'] ?></span>
                                            <input name="grup-<?php echo $grup['id'] ?>" id="grup-<?php echo $grup['id'] ?>" type="hidden" value="<?php if(in_array($grup['id'],$grupsSelected)){ echo "1"; }else {echo "0";} ?>">
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                            <input type="hidden" name="accio" value="editar" />
                            <input type="hidden" name="id" value="<?php echo $activitat[0]['id'] ?>" />
                            <div style="margin:0px;" class="row">
                                <div class="col-3">
                                    <br/>
                                    <div class="form-group">
                                        <input class="form-control" type="submit" value="Guardar">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="card-footer ">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php require_once('footer.php') ?>
<?php require_once('./assets/js/formularis.js.php') ?>