

            <script>
  $.datepicker.regional['cat'] = {
      closeText: 'Tancar', // set a close button text
      currentText: 'Avui', // set today text
      monthNames: ['Gener','Febrer','Març','Abril','Maig','Juny','Juliol','Agost','Setembre','Octubre','Novembre','Desembre'], // set month names
      monthNamesShort: ['Gen','Feb','Mar','Abr','Mai','Jun','Jul','Ago','Set','Oct','Nov','Des'], // set short month names
      dayNames: ['Diumenge','Dilluns','Dimarts','Dimercres','Dijous','Divendres','Dissabte'], // set days names
      dayNamesShort: ['Diu','Dil','Dima','Dime','Dijo','Div','Dis'], // set short day names
      dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Ve','Sa'], // set more short days names
      dateFormat: 'yy-mm-dd' // set format date
  };

  $.datepicker.setDefaults(
    $.extend(
      $.datepicker.regional['cat'],
      {yearRange: "-100:+0"}
    )
  ) 
  
  
    $( "#data-activitat" ).datepicker({
        changeMonth: true,
        changeYear: true
    });
    $( "#inici-inscripcions" ).datepicker({
        changeMonth: true,
        changeYear: true
    });
    $( "#fi-inscripcions" ).datepicker({
        changeMonth: true,
        changeYear: true
    });
 
</script>
