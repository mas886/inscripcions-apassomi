function crearGrupModal() {
    openModalInputs('Nou grup', 'Introdueix el nom del nou grup, i la posició en valor numèric que ocuparà.', [['Nom del grup', 'nomGrup', 'required maxlength="150"', 'text', 'Introdueix nom...', ''], ['Ordre d\'aparició', 'ordreGrup', 'required', 'number', 'Ordre en que apareixerà...', '0'], ['Visibilitat', 'visibleGrup', 'required', 'select', [["1", "Si"], ["0", "No"]], '0']], [['Cancelar', 'hideModal()', 'red'], ['Guardar', 'crearGrup()', 'green validar']]);
}

function crearGrup(grupId = "") {
    $(".validar").prop('disabled', true);
    if (validarGrupForm()) {
        console.log(modalInputsGetValues());
        $(".modalInputs").submit();
    } else {
        $(".validar").prop('disabled', false);
    }
}

function esborrarGrup(grupId) {
    $.ajax({
        type: 'POST',
        url: './controller/ajax.php',
        data: {
            'action': 'grupDelete',
            'grupId': grupId
        },
        success: function (msg) {
            data = JSON.parse(msg);
            if (data.ERR == 0) {
                if (window.history.replaceState) {
                    window.history.replaceState(null, null, window.location.href);
                }
                location.reload();
            } else {
                openModalDefault('Error!', data.MSG, []);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Ei!");
        }
    });
}

function validarGrupForm() {
    isValid = true;
    if ($('input[name="nomGrup"]').val() == "") {
        $('input[name="nomGrup"]').addClass("error");
        isValid = false;
    }
    if ($('input[name="ordreGrup"]').val() == "" || isNaN($('input[name="ordreGrup"]').val())) {
        $('input[name="ordreGrup"]').addClass("error");
        isValid = false;
    }
    if (isValid) {
        $('input').removeClass("error");
    }
    return isValid;
}

function editarGrupModal(id, buttonId) {
    botoText = $('#' + buttonId).html();
    if ($('#' + buttonId).html() != '<div class="modalInlineSpinner"></div>') {
        $('#' + buttonId).html('<div class="modalInlineSpinner"></div>');
        $.ajax({
            type: 'POST',
            url: './controller/ajax.php',
            data: {
                'action': 'grupData',
                'grupId': id
            },
            success: function (msg) {
                $('#' + buttonId).html(botoText);
                data = JSON.parse(msg)[0];
                openModalInputs('Editar grup', 'Edita el nom i l\'ordre d\'aparició del grup.', [['Nom del grup', 'nomGrup', 'required maxlength="150"', 'text', 'Introdueix nom...', data.nom], ['Ordre d\'aparició', 'ordreGrup', 'required', 'number', 'Ordre en que apareixerà...', data.ordre], ['Visibilitat', 'visibleGrup', 'required', 'select', [["1", "Si"], ["0", "No"]], data.visible], ['', 'idGrup', '', 'hidden', '', data.id]], [['Cancelar', 'hideModal()', 'red'], ['Guardar', 'crearGrup()', 'green validar']]);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $('#' + buttonId).html(botoText);
            }
        });
    }
}

function borrarGrupModal(id, buttonId) {
    openModalDefault('Esborrar grup', 'Estàs segur que vols esborrar aquest grup? Pensa-t\'ho bé abans de fer-ho...', [['Esborrar', 'esborrarGrup(' + id + ')', 'red'], ['Cancelar', 'hideModal()', 'green validar']]);
}

function desvincularTotsModal() {
    openModalDefault('Alerta!', 'Utilitzant aquesta opció es desvincularan <strong>tots</strong> els usuaris de les activitats de les que formen part. Utilitza l\'opció únicament en cas que siga necessari.', [['Desvincular', 'desvincularTots()', 'red validar'], ['Cancelar', 'hideModal()', 'green']]);
}

function desvincularTots() {
    $("#modalButton.validar").html('<div class="modalInlineSpinner"></div>');
    $("#modalButton.validar").attr('disabled', 'disabled');
    $('select[name="grup"]').attr('disabled', 'disabled');
    $.ajax({
        type: 'POST',
        url: './controller/ajax.php',
        data: {
            'action': 'desvincularTots'
        },
        success: function (msg) {
            data = JSON.parse(msg);
            if(data.ERR==0){
                hideModal();
                openModalDefault('Borrat correctament', "S'han desvinculat tots els participants de les seves activitats.", [['Tancar', 'hideModal()', 'green']]);
            }else{
                hideModal();
                openModalDefault('Error!', "No ha estat possible realitzar l'acció.", [['Tancar', 'hideModal()', 'red']]);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            hideModal();
            openModalDefault('Error!', "Hi ha hagut un error inesperat.", [['Tancar', 'hideModal()', 'red']]);
        }
    });
}

function getPageParticipants(pageWanted, regAmount = "", ordre = "", busca = "") {
    if (busca == "") {
        busca = $("#defBusca").val();
    } else {
        $("#defBusca").val(busca);
    }
    if (regAmount == "") {
        regAmount = $("#defRegistres").val();
    } else {
        $("#defRegistres").val(regAmount);
        $("#amountRegs").html(regAmount);
    }
    if (ordre == "") {
        ordre = $("#defOrdenacio").val();
    } else {
        $("#defOrdenacio").val(ordre);
    }
    $(".tableLoadingDiv").show(0);
    $.ajax({
        type: 'POST',
        url: './controller/ajax.php',
        data: {
            'action': 'getTableParticipants',
            'pageWanted': pageWanted,
            'regAmount': regAmount,
            'ordre': ordre,
            'busca': busca,

        },
        success: function (msg) {
            data = JSON.parse(msg);
            if (data.ERR == 0) {
                $("#curPage").val(pageWanted);
                $("#participantsTable").html(data.CONT);
                $("#paginationButtons").html(data.PAGS);
                $(".buttonPaginationItem").removeClass("actual");
                $(".buttonPaginationItem").removeAttr("disabled");
                $(".botPag-" + pageWanted).addClass("actual");
                $(".botPag-" + pageWanted).attr("disabled", "");
                checkPaginationButtons();
                $(".tableLoadingDiv").hide(0);
                $("#totalRegs").html(data.TOT);
            } else {
                $("#participantsTable").html("");
                $("#paginationButtons").html("");
                $('.loadingTableText').html(data.MSG);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $('.loadingTableText').html("Hi ha hagut un problema... Recarrega la pàgina.");
        }
    });
}

function getNextParticipants() {
    getPageParticipants(parseInt($("#curPage").val()) + 1);
}

function getPrevParticipants() {
    getPageParticipants(parseInt($("#curPage").val()) - 1);
}

function checkPaginationButtons() {
    if ($(".buttonPaginationItem:last-child").html() == (parseInt($("#curPage").val()) + 1)) {
        $(".buttonPagNext").attr("disabled", "");
    } else {
        $(".buttonPagNext").removeAttr("disabled");
    }
    if ((parseInt($("#curPage").val())) == 0) {
        $(".buttonPagPrev").attr("disabled", "");
    } else {
        $(".buttonPagPrev").removeAttr("disabled");
    }
}

function filtrarParticipants() {
    $("#defOrdenacio").val($("#partOrd").val());
    $("#defBusca").val($("#partSearch").val());
    getPageParticipants(0, $("#partAmount").val(), $("#partOrd").val(), $("#partSearch").val());
}

function carregarParticipantsModal() {
    $.ajax({
        type: 'POST',
        url: './controller/ajax.php',
        data: {
            'action': 'getGrups'
        },
        success: function (msg) {
            data = JSON.parse(msg);
            if (data.ERR == 0) {
                grups = [];
                data.MSG.forEach(element => {
                    grups.push([element.id, element.nom]);
                });
                openModalInputs('Carregar participants', "Assegura't que l'arxiu en CSV està ben formatat <i>Nom;Cognoms;Data de Naixement (FORMAT d/m/Y 0:00:00);Telèfon Altre Contacte;Telèfon Fix;Telèfon Mobil Mare/Tutora;Telèfon Mòbil Pare/Tutor</i> el CSV no ha d'incloure en cap cas cometes ni cap altre separador a part del ;", [['Grup', 'grup', 'required', 'select', grups, ""], ['Llistat de participants', 'participants', 'required', 'file', 'Carrega llistat de participants...', '.csv']], [['Cancelar', 'hideModal()', 'red'], ['Carregar', 'carregarParticipants()', 'green validar']]);
            } else {
                openModalDefault('Error!', data.MSG, [['Tancar', 'hideModal()', 'red']]);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $('#' + buttonId).html(botoText);
        }
    });
}

function carregarParticipants() {
    if (validarCarregarParticipantsForm()) {
        $("#modalButton.validar").html('<div class="modalInlineSpinner"></div>');
        $("#modalButton.validar").attr('disabled', 'disabled');
        $('select[name="grup"]').attr('disabled', 'disabled');
        $('input[name="participants"]').attr('disabled', 'disabled');
        var file = document.getElementById('participants').files[0]; //Files[0] = 1st file
        if(file.type == "application/vnd.ms-excel" || file.type == "text/csv" || file.size > 524288 ){
            var reader = new FileReader();
            reader.readAsText(file, 'UTF-8');
            reader.onload = function (event) {
                $.ajax({
                    type: 'POST',
                    url: './controller/ajax.php',
                    data: {
                        'action': 'carregarParticipants',
                        'participants': event.target.result,
                        'grupId': $('select[name="grup"]').val()
                    },
                    success: function (msg) {
                        data = JSON.parse(msg);
                        if (data.ERR == 0) {
                            window.location.href = "./participantsValidar.php?batchId=" + data.MSG;
                        } else {
                            hideModal();
                            openModalDefault('Error!', data.MSG, [['Tancar', 'hideModal()', 'red']]);
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        hideModal();
                        openModalDefault('Error!', "Hi ha hagut un error inesperat.", [['Tancar', 'hideModal()', 'red']]);
                    }
                });
            };
        }else{
            hideModal();
            openModalDefault('Error!', "L'arxiu seleccionat no és correcte o massa gran, assegura't que que és de tipus CSV.", [['Tancar', 'hideModal()', 'red']]);
        }
    }
}

function validarCarregarParticipantsForm() {
    isValid = true;
    if ($('select[name="grup"]').val() == "") {
        $('select[name="grup"]').addClass("error");
        isValid = false;
    }
    if ($('input[name="participants"]').val() == "") {
        $('input[name="participants"]').addClass("error");
        isValid = false;
    }
    if (isValid) {
        $('input').removeClass("error");
    }
    return isValid;
}

function validarParticipantsLotModal() {
    openModalDefault('Validar valors', "Estàs segur que vols validar els participants pujats? Assegura't de revisar el lots a consciència.", [['Tancar', 'hideModal()', 'red'], ['Validar', 'validarParticipantsLot()', 'green validar']]);
}

function validarParticipantsLot() {
    $("#modalButton.validar").html('<div class="modalInlineSpinner"></div>');
    $("#modalButton.validar").attr('disabled', 'disabled');
    $('select[name="grup"]').attr('disabled', 'disabled');
    $.ajax({
        type: 'POST',
        url: './controller/ajax.php',
        data: {
            'action': 'validarParticipants',
            'remesaId': $("#batchId").val()
        },
        success: function (msg) {
            data = JSON.parse(msg);
            if (data.ERR == 0) {
                window.location.href = "./grupsDetall.php?id=" + data.MSG;
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            hideModal();
            openModalDefault('Error!', "Hi ha hagut un error inesperat.", [['Tancar', 'hideModal()', 'red']]);
        }
    });
}

function treureParticipantModal(participantId, grupId){
    openModalDefault('Estàs segur?', "Desvincularàs un participant de l'activitat.", [['Tancar', 'hideModal()', 'red'],['Confirmar', 'treureParticipant('+participantId+','+grupId+')', 'green validar']]);
}

function treureParticipant(participantId, grupId){
    $("#modalButton.validar").html('<div class="modalInlineSpinner"></div>');
    $("#modalButton.validar").attr('disabled', 'disabled');
    $('select[name="grup"]').attr('disabled', 'disabled');
    $.ajax({
        type: 'POST',
        url: './controller/ajax.php',
        data: {
            'action': 'treureParticipant',
            'participantId': participantId,
            'grupId': grupId
        },
        success: function (msg) {
            data = JSON.parse(msg);
            if(data.ERR==0){
                $("#grupPartTable").html(data.MSG);
                $("#participantsNum").html($("#grupPartTable tr").length);
                hideModal();
                openModalDefault('Borrat correctament', "", [['Tancar', 'hideModal()', 'green']]);
            }else{
                hideModal();
                openModalDefault('Error!', "No ha estat possible treure el participant del grup.", [['Tancar', 'hideModal()', 'red']]);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            hideModal();
            openModalDefault('Error!', "Hi ha hagut un error inesperat.", [['Tancar', 'hideModal()', 'red']]);
        }
    });
}

function treureActivitatModal(activitatId, grupId){
    openModalDefault('Estàs segur?', "Desvincularàs l'activitat del grup.", [['Tancar', 'hideModal()', 'red'],['Confirmar', 'treureActivitat('+activitatId+','+grupId+')', 'green validar']]);
}

function treureActivitat(activitatId, grupId){
    $("#modalButton.validar").html('<div class="modalInlineSpinner"></div>');
    $("#modalButton.validar").attr('disabled', 'disabled');
    $('select[name="grup"]').attr('disabled', 'disabled');
    $.ajax({
        type: 'POST',
        url: './controller/ajax.php',
        data: {
            'action': 'treureActivitat',
            'activitatId': activitatId,
            'grupId': grupId
        },
        success: function (msg) {
            data = JSON.parse(msg);
            if(data.ERR==0){
                $("#grupActTable").html(data.MSG);
                $("#activitatsNum").html($("#grupActTable tr").length);
                hideModal();
                openModalDefault('Borrat correctament', "", [['Tancar', 'hideModal()', 'green']]);
            }else{
                hideModal();
                openModalDefault('Error!', "No ha estat possible treure el participant del grup.", [['Tancar', 'hideModal()', 'red']]);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            hideModal();
            openModalDefault('Error!', "Hi ha hagut un error inesperat.", [['Tancar', 'hideModal()', 'red']]);
        }
    });
}

function desvincularParticipantsModal(){
    openModalDefault('Alerta!', "Desvincularàs <strong>tots</strong> els participant de l'activitat. Una vegada fet això ja no es podran apuntar a les noves activitats, aquesta acció no es pot desfer.", [['Tancar', 'hideModal()', 'green'],['Desvincular', 'desvincularParticipants('+$("#grupId").val()+')', 'red validar']]);
}

function desvincularParticipants(grupId){
    $("#modalButton.validar").html('<div class="modalInlineSpinner"></div>');
    $("#modalButton.validar").attr('disabled', 'disabled');
    $('select[name="grup"]').attr('disabled', 'disabled');
    $.ajax({
        type: 'POST',
        url: './controller/ajax.php',
        data: {
            'action': 'desvincularParticipants',
            'grupId': grupId
        },
        success: function (msg) {
            data = JSON.parse(msg);
            if(data.ERR==0){
                $("#grupPartTable").html(data.MSG);
                $("#participantsNum").html($("#grupPartTable tr").length);
                hideModal();
                openModalDefault('Participants desvinculats', "", [['Tancar', 'hideModal()', 'green']]);
            }else{
                hideModal();
                openModalDefault('Error!', "No ha estat possible desvincular els participants.", [['Tancar', 'hideModal()', 'red']]);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            hideModal();
            openModalDefault('Error!', "Hi ha hagut un error inesperat.", [['Tancar', 'hideModal()', 'red']]);
        }
    });
}

function selectGrupCrear(grupId){
    if($("#grupSel-"+grupId).hasClass("selected")){
        $("#grupSel-"+grupId).removeClass("selected")
        $("#grup-"+grupId).val(0);
    }else{
        $("#grupSel-"+grupId).addClass("selected")
        $("#grup-"+grupId).val(1);
    }
}

function esborrarActivitatModal(activitatId){
    openModalDefault('Alerta!', "Si borres l'activitat, aquesta ja no es podrà recuperar.", [['Tancar', 'hideModal()', 'red'],['Confirmar', 'esborrarActivitat('+activitatId+')', 'green validar']]);
}

function esborrarActivitat(activitatId){
    $("#modalButton.validar").html('<div class="modalInlineSpinner"></div>');
    $("#modalButton.validar").attr('disabled', 'disabled');
    $('select[name="grup"]').attr('disabled', 'disabled');
    $.ajax({
        type: 'POST',
        url: './controller/ajax.php',
        data: {
            'action': 'esborrarActivitat',
            'activitatId': activitatId
        },
        success: function (msg) {
            data = JSON.parse(msg);
            if(data.ERR==0){
                hideModal();
                openModalDefault('Activitat Borrada', "", [['Tornar Enrere', 'window.location.href = \'./index.php\'', 'green']]);
            }else{
                hideModal();
                openModalDefault('Error!', data.MSG, [['Tancar', 'hideModal()', 'red']]);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            hideModal();
            openModalDefault('Error!', "Hi ha hagut un error inesperat.", [['Tancar', 'hideModal()', 'red']]);
        }
    });
}