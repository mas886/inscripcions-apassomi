<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="robots" content="noindex">
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="../assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Admin inscripcions Grup d'Esplai Apassomi</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="../vendor/fontawesome-free-5.2.0-web/css/all.css">
    <!-- CSS Files -->
    <link href="./assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="./assets/css/light-bootstrap-dashboard.css?v=2.0.1" rel="stylesheet" />
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="./assets/css/demo.css" rel="stylesheet" />
    <link href="./css/custom.css" rel="stylesheet" />
    <link href="./helpers/modalCreator/modalCreator.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="../vendor/jquery-ui-1.12.1.custom/jquery-ui.css">
    
    <?php 
        require_once("../config.php");
        require_once("../controller/functions.php");
        generateConnection();
        
        session_start();
        
        if(!isset($_SESSION['user']) || !isset($_SESSION['password']) || !isset($_SESSION['expire_session']) || !($_SESSION['expire_session']>time()) || !checkAdminLogin($_SESSION['user'],$_SESSION['password']))
            header("Location: ./logout.php");
        
    ?>
    
</head>

<body>
    <div class="wrapper">
        <div class="sidebar" data-image="./assets/img/sidebar-5.jpg">
            <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | blue | green | orange | red"

        Tip 2: you can also add an image using data-image tag
    -->
            <div class="sidebar-wrapper">
                <div class="logo">
                    <a href="/" class="simple-text">
                        INSCRIPCIONS APASSOMI
                    </a>
                </div>
                <ul class="nav">
                    <li class="nav-item">
                        <a class="nav-link" href="index.php">
                            <i class="nc-icon nc-atom"></i>
                            <p>Activitats</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="grups.php">
                            <i class="nc-icon nc-grid-45"></i>
                            <p>Grups</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="participants.php">
                            <i class="nc-icon nc-single-02"></i>
                            <p>Participants</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="main-panel">
            <!-- Navbar -->
            <nav class="navbar navbar-expand-lg " color-on-scroll="500">
                <div class=" container-fluid  ">
                    <a class="navbar-brand" href="./"> Panell </a>
                    <button href="" class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-bar burger-lines"></span>
                        <span class="navbar-toggler-bar burger-lines"></span>
                        <span class="navbar-toggler-bar burger-lines"></span>
                    </button>
                    <div class="collapse navbar-collapse justify-content-end" id="navigation">
                        <ul class="nav navbar-nav mr-auto">
                            <li class="nav-item">
                                <a href="#" class="nav-link" data-toggle="dropdown">
                                    <i class="nc-icon nc-atom"></i>
                                    <span class="d-lg-none">Panell</span>
                                </a>
                            </li>
                        </ul>
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item">
                                <a class="nav-link" href="./logout.php">
                                    <span class="no-icon">Log out</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav> 
