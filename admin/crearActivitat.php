<?php require_once("header.php");

$query = "SELECT * FROM `grup` WHERE `visible`=1 ORDER BY `ordre` ASC";
$grupsActius = getQuery($query, []);


if (isset($_POST['nom']) && isset($_POST['data']) && isset($_POST['datainici']) && isset($_POST['datafi']) && isset($_POST['preu'])) {
    if ($_POST['nom'] != '' && $_POST['data'] != '' && $_POST['datainici'] != '' && $_POST['datafi'] != '' && $_POST['preu'] != '') {
        if (isset($_POST['visible'])) {
            $_POST['visible'] = 1;
        } else {
            $_POST['visible'] = 0;
        }

        $query = "INSERT INTO `activitat`(`nom`, `preu`, `visible`, `data`, `inici_inscripcions`, `final_inscripcions`) VALUES (:nom, :preu, :visible, :data, :datainici, :datafi)";
        $activitat = executeQuery($query, [':nom' => $_POST['nom'], ':preu' => $_POST['preu'], ':visible' => $_POST['visible'], ':data' => $_POST['data'], ':datainici' => $_POST['datainici'], ':datafi' => $_POST['datafi']], true);

        if (is_numeric($activitat)) {
            foreach($grupsActius as $grup){
                if(isset($_POST["grup-".$grup['id']]) && $_POST["grup-".$grup['id']]==1){
                    $query="INSERT INTO `activitat_grup`(`grup_id`, `activitat_id`) VALUES (:grupId,:activitatId)";
                    executeQuery($query, [':grupId' => $grup['id'], ':activitatId' => $activitat]);
                }
            }
            echo "Redireccionant a la pàginia d'inici.";
            echo ("<script>window.location.href = \"./index.php\";</script>");
            exit;
        }
    }
}

?>

<!-- End Navbar -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card ">
                    <div style="margin:0px;" class="card-header row">
                        <div class="col-8">
                            <h4 class="card-title">Crear Activitat</h4>
                            <p class="card-category">Afegeix una activitat nova</p>
                        </div>
                        <div class="col-4 text-right" style="padding-right: 0px;">
                            <a class="icon-big" href="./"><i class="fas fa-arrow-left"></i></a>
                        </div>
                    </div>
                    <div class="card-body ">
                        <form method="POST" target="_self">
                            <div style="margin:0px;" class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Nom Activitat</label>
                                        <input required class="form-control" name="nom" placeholder="Nom activitat..." type="text">
                                    </div>
                                </div>
                            </div>
                            <div style="margin:0px;" class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Data Activitat</label>
                                        <input required id="data-activitat" class="form-control" name="data" placeholder="Data de realitzacio.." type="text">
                                    </div>
                                </div>
                            </div>
                            <div style="margin:0px;" class="row">
                                <div class="col-md-6 pr-1">
                                    <div class="form-group">
                                        <label>Inici d'inscripcions</label>
                                        <input required id="inici-inscripcions" class="form-control" placeholder="Data d'inici de les inscripcions" name="datainici" type="text">
                                    </div>
                                </div>
                                <div class="col-md-6 pl-1">
                                    <div class="form-group">
                                        <label>Fi d'inscripicons</label>
                                        <input required id="fi-inscripcions" class="form-control" placeholder="Data de fi de les inscripcions" name="datafi" type="text">
                                    </div>
                                </div>
                            </div>
                            <div style="margin:0px;" class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Preu</label>
                                        <input required class="form-control" name="preu" placeholder="Preu de l'activitat..." step=".01" type="number">
                                    </div>
                                </div>
                            </div>
                            <div style="margin:0px;" class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Es visible</label>
                                        <input name="visible" style="vertical-align: middle;" type="checkbox" checked />
                                    </div>
                                </div>
                            </div>
                            <div style="margin:0px;" class="row">
                                <div class="col-md-12">
                                    <h4 class="card-title">Grups</h4>
                                    <p class="card-category">Selecciona, fent clic, els grups on l'activitat estarà disponible. Es pot seleccionar més d'un grup.</p>
                                </div>
                            </div>
                            <div style="margin:0px;" class="row">
                                <?php foreach ($grupsActius as $grup) { ?>
                                    <div class="col-12 col-sm-6 col-md-3 grupSelectorHolder">
                                        <div id="grupSel-<?php echo $grup['id'] ?>" class="grupSelector" onclick="selectGrupCrear(<?php echo $grup['id'] ?>)">
                                            <span><?php echo $grup['nom'] ?></span>
                                            <input name="grup-<?php echo $grup['id'] ?>" id="grup-<?php echo $grup['id'] ?>" type="hidden" value="0">
                                        </div>
                                    </div>
                                <?php } ?>

                            </div>
                            <div style="margin:0px;" class="row">
                                <div class="col-3">
                                    <br/>
                                    <div class="form-group">
                                        <input class="form-control" type="submit" value="Guardar">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="card-footer ">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php require_once('footer.php') ?>
<?php require_once('./assets/js/formularis.js.php') ?>