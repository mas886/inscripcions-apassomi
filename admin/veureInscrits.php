<?php require_once("header.php"); 

if(isset($_GET['id']) && $_GET['id']!='' && is_numeric($_GET['id'])){
    $query="SELECT * FROM `activitat` WHERE id=:id";
    $activitat=getQuery($query,[":id"=>$_GET['id']]);
    if(!$activitat){
        header("Location: ./");
    }else{
        if(isset($_POST['participant'])){
            $query="DELETE FROM `infant_activitat` WHERE `infant_id`=:infantId AND `activitat_id`=:activitatId";
            $res=executeQuery($query,[':infantId'=>$_POST['participant'],':activitatId'=>$_GET['id']]);
        }
        $query="SELECT i.id, i.cognoms, i.nom, CONCAT(t.nom,' ',t.cognoms) as 'tutor',`data_inscripció` FROM `infant_activitat` ia JOIN `infant` i ON i.id=ia.`infant_id`  JOIN `tutor` t ON t.`id`=ia.`tutor_id` WHERE `activitat_id`=:id ORDER BY `cognoms` ASC";
        $inscrits=getQuery($query,[":id"=>$_GET['id']]);
        

        
    }
}else{
    header("Location: ./");
}


?>

            <!-- End Navbar -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card ">
                                <div style="margin:0px;" class="card-header row">
                                    <div class="col-8">
                                        <h4 class="card-title">Inscrits a <?php echo $activitat[0]['nom'] ?></h4>
                                        <p class="card-category">Estàs veient els inscrits en aquesta activitat</p>
                                    </div>
                                    <div class="col-4 text-right" style="padding-right: 0px;">
                                        <a class="icon-big" href="./"><i class="fas fa-arrow-left"></i></a>
                                    </div>
                                </div>
                                <div class="card-body ">
                                    <?php if(isset($res) && $res){ ?>
                                    <div class="alert alert-success">
                                        <button type="button" aria-hidden="true" class="close" data-dismiss="alert">
                                            <i class="nc-icon nc-simple-remove"></i>
                                        </button>
                                        <span>
                                            <b> Èxit - </b> S'ha borrat el participant correctament.</span>
                                    </div>
                                    <?php }else if(isset($res) && !$res){ ?>
                                    <div class="alert alert-danger">
                                        <button type="button" aria-hidden="true" class="close" data-dismiss="alert">
                                            <i class="nc-icon nc-simple-remove"></i>
                                        </button>
                                        <span>
                                            <b> Error - </b> Alguna cosa ha fallat al borrar el participant.</span>
                                    </div>
                                    <?php } ?>
                                <table class="table table-hover table-striped">
                                        <thead>
                                            <tr><th>Num.</th>
                                            <th>Cognoms</th>
                                            <th>Nom</th>
                                            <th>Tutor</th>
                                            <th>Data d'inscripció</th>
                                            <th>&nbsp;</th>
                                        </tr></thead>
                                        <tbody>
                                            <?php $c=0; foreach($inscrits as $inscrit){ $c++; ?>
                                                <tr>
                                                    <td>
                                                        <?php echo $c; ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $inscrit['cognoms']; ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $inscrit['nom']; ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $inscrit['tutor']; ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $inscrit['data_inscripció']; ?>
                                                    </td>
                                                    <td>
                                                        <form target="_SELF" method="POST">
                                                            <input type="hidden" name="participant" value="<?php echo $inscrit['id'] ?>">
                                                            <button type="submit" class="btn btn-danger btn-fill">
                                                                <i class="trash-can fas fa-trash-alt"></i>
                                                            </button>
                                                        </form>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="card-footer ">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<?php require_once('footer.php') ?>
<?php require_once('./assets/js/formularis.js.php') ?>
 
