<?php require_once("header.php"); 

$query="SELECT * FROM `activitat` WHERE 1 ORDER BY `id`DESC";
$activitats=getQuery($query,[]);
?>

            <!-- End Navbar -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card ">
                                <div style="margin:0px;" class="card-header row">
                                    <div class="col-8">
                                        <h4 class="card-title">Activitats</h4>
                                        <p class="card-category">Llistat d'activitats creades</p>
                                    </div>
                                    <div class="col-4 text-right" style="padding-right: 0px;">
                                        <a class="btn btn-success btn-fill" href="./crearActivitat.php">
                                                            Crear
                                                        </a>
                                    </div>
                                </div>
                                <div class="card-body "><div class="table-full-width">
                                        <table class="table">
                                            <tbody>
                                                <?php foreach($activitats as $activitat){ ?>
                                                
                                                <tr>
                                                    <td style="width: 100%;"><?php echo $activitat['nom'] ?></td>
                                                    <td class="td-actions text-right">
                                                        <a download="<?php echo $activitat['nom']; ?>.csv" class="btn btn-success btn-fill" href="./generarLlistat.php?id=<?php echo $activitat['id']; ?>">
                                                            <i class="fas fa-file-download"></i>
                                                        </a>
                                                        &nbsp;
                                                        <a class="btn btn-info btn-fill" href="./veureInscrits.php?id=<?php echo $activitat['id']; ?>">
                                                            Inscrits
                                                        </a>
                                                        &nbsp;
                                                        <a class="btn btn-warning btn-fill" href="./editarActivitat.php?id=<?php echo $activitat['id']; ?>">
                                                            Editar
                                                        </a>
                                                    </td>
                                                </tr>
                                                
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="card-footer ">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<?php require_once('footer.php') ?>
