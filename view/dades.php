<div class="container">
  <div class="row">
    <div class="col-12 col-md-6 center-div">
      <br/>
      <h2 class="align-center">Configuració</h2>
      <p class="align-center">Actualitza o modifica les dades de contacte</p>
      <form method="POST" id="update_form">
        <input class="width-100" type="text" id="nom" name="nom" placeholder="Nom *" value="<?php echo $GLOBALS['tutor']->nom; ?>" required/>
        <input class="width-100" type="text" id="cognoms" name="cognoms" placeholder="Cognoms *" value="<?php echo $GLOBALS['tutor']->cognoms; ?>" required/>
        <input class="width-100" type="text" id="dni" name="dni" placeholder="DNI *" maxlength="9" value="<?php echo $GLOBALS['tutor']->dni; ?>" required/>
        <span class="error-mes" id="error-dni" style="display:none">DNI o NIE incorrecte comprovi errors i que no hi hagi espais.</span>
        <input id="datalogin" class="width-100" type="text" name="data" placeholder="Data de naixement *" value="<?php echo $GLOBALS['tutor']->data_naix; ?>" required readonly="true"/>
        <span class="error-mes" id="error-data" style="display:none">Format incorrecte, s'accepta any-mes-dia ex: 2015-02-31</span>
        <input class="width-100" type="text" id="email" name="email" placeholder="Correu electrònic *" value="<?php echo $GLOBALS['tutor']->correu; ?>" required/>
        <span class="error-mes" id="error-email" style="display:none">Format de l'email incorrecte</span>
        <p class="small-text">* Es important que les dades siguin correctes</p>
        <button class="align-center" id="boto-actualitza" type="submit">Actualitza</button>
      </form>
    </div>
  </div>
</div>
