<?php
$infant = new Infant;
if (isset($_POST['params']) && $infant->load($_POST['params']) && $infant->belongsToTutor($tutor->id)) {
  // :)
  $activitatsInscrit = [];
  $activitats = $infant->getActivitats();
  foreach ($activitats as $activitat) {
    $activitatsInscrit[] = $activitat['id'];
  }
} else {
  echo "Error"; ?>

  <div class="container">
    <div class="row">
      <div class="col-12 col-md-6 center-div">
        <br />
        <h2 class="align-center">Error</h2>
        <p class="align-center">Algú està fent el que no toca.</p>
      </div>
    </div>
  </div>

<?php
  exit;
}

?>

<div class="container">
  <div class="row">
    <div class="col-12 col-md-6 center-div">
      <br />
      <h2 class="align-center">Inscriure</h2>
      <p class="align-center">Activitats disponibles per a <?php echo $infant->nom . " " . $infant->cognoms ?></p>
      <br />
      <?php $activitat = new Activitat;
      $activitats = $activitat->getActivitatsParticipant($infant->id);
      $cont = 0;
      ?>
      <?php foreach ($activitats as $activitat) {
        if (!in_array($activitat['id'], $activitatsInscrit)) {
          $cont++;
          $buttonId = "butt-" . $cont;
      ?>
          <div class="infant-card">
            <h4 class="align-center"><?php echo $activitat['nom'] ?></h4>
            <div class="container">
              <div class="row">
                <div class="col-md-6 col-12 center-mobile">
                  <p class=""><strong>Inscripcions del:</strong><br />
                    <?php echo substr($activitat['inici_inscripcions'], 0, 10); ?> al <?php echo substr($activitat['final_inscripcions'], 0, 10); ?>
                  </p>
                </div>
                <div class="col-md-6 col-12 center-mobile">
                  <p class=""><strong>Grup:</strong><br />
                    <?php echo $activitat['grup_nom'] ?>
                  </p>
                </div>
              </div>
            </div>
            <div class="container">
              <div class="row">
                <div class="col-md-6 col-12 center-mobile">
                  <strong>Data:</strong> <?php echo $activitat['data']; ?>
                  <br />
                  <strong>Preu:</strong> <?php echo $activitat['preu']; ?>€
                </div>
                <div class="col-md-6 col-12 align-center">
                  <br class="d-inline d-md-none"/>
                  <button id="<?php echo $buttonId ?>" onClick="inscriuActivitatParticipant('<?php echo $activitat['id']; ?>','<?php echo $_POST['params'] ?>','<?php echo $buttonId ?>')" class="button-100">Inscriu</button>
                </div>
              </div>
            </div>
          </div>
          <br />
        <?php }
      }
      if ($cont == 0) {
        ?>
        <hr />
        <p class="align-center text-darkgrey">Ara mateix no hi ha cap activitat disponible per aquest participant.</p>
      <?php }
      ?>
    </div>
  </div>
</div>