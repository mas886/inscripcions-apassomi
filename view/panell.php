<span id="show-logout" style="display:none;"></span>
<div class="container">
  <div class="row">
    <div class="col-12 col-md-6 center-div">
      <br/>
      <h2 class="align-center">Panell</h2>
      <p class="align-center">Benvingut, <?php echo $tutor->nom; ?></p>
      <br/>
      <div class="container no-padding ">
        <div class="row">
          <div class="col-md-6 col-sm-12 col-12">
            <button onClick="showPage('infants')" class="button-100">Afegir Participants</button>
            <br/>
          </div>
          <div class="col-md-6 col-sm-12 col-12">
            <button onClick="showPage('dades')" class="button-100">Configuració</button>
            <br/>
          </div>
        </div>
      </div>
      <br/><br/>
      <h3 class="align-center">Participants</h3>
      <br/>
      <?php 
      $infants=$tutor->getInfants();
      foreach($infants as $infant){ ?>
        <div class="infant-card">
          <div class="avatar"><?php echo strtoupper(mb_substr($infant->nom, 0, 1).mb_substr($infant->cognoms, 0, 1)); ?></div>
          <p class="card-nom"><?php echo $infant->nom." ".$infant->cognoms ?></p>
          <p><strong>Activitats:</strong></p>
            <div class="row">
              <div class="col-md-6 col-12">
                <button class="button-100" onClick="showPage('inscripcions','<?php echo $infant->id ?>')">Inscriure</button>
                <div class="d-block d-sm-block d-md-none"><br/></div>
              </div>
              <div class="col-md-6 col-12">
                <button class="button-100" onClick="showPage('activitats','<?php echo $infant->id ?>')">Històric</button>
              </div>
            </div>
        </div>
        <br/>
      <?php } if(sizeof($infants)==0){?>
        <hr/>
        <p class="align-center text-darkgrey">Actualment no estàs a càrrec de cap participant, per afegir-ne clica al botó "Afegir Participants".</p>
      <?php } ?>
      <br/>
      <br/>
    </div>
  </div>
</div>
