<div class="container">
  <div class="row">
    <div class="col-12 col-md-6 center-div">
      <br/>
      <h2 class="align-center">Identifica't</h2>
      <p class="align-center">Dades del pare, mare o tutor</p>
      <br/>
      <form method="POST" id="login_form">
        <input class="width-100" type="text" id="dni" name="dni" placeholder="DNI" maxlength="9" required/>
        <input id="datalogin" class="width-100" type="text" name="data" placeholder="Data de naixement" required readonly />
        <span class="error-mes" style="display:none">Format incorrecte, s'accepta any-mes-dia ex: 2015-02-31</span>
        <a href="#" onClick="showPage('registre')">Registra't</a>
        <br/><br/>
        <button class="align-center" id="boto-login" type="submit">Entrar</button>
      </form>
      <br/>
      <br/>
    </div>
  </div>
</div>
