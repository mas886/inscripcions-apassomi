<?php
$infant=new Infant;
if (isset($_POST['params']) && $infant->load($_POST['params']) && $infant->belongsToTutor($tutor->id)){
  // :)
}else{
  echo "Error"; ?>

  <div class="container">
    <div class="row">
      <div class="col-12 col-md-6 center-div">
        <br/>
        <h2 class="align-center">Error</h2>
        <p class="align-center">Algú està fent el que no toca.</p>
      </div>
    </div>
  </div>

  <?php
  exit;
}

 ?>

<div class="container">
  <div class="row">
    <div class="col-12 col-md-6 center-div">
      <br/>
      <h2 class="align-center">Històric</h2>
      <p class="align-center">Llistat d'activitats en que <?php echo $infant->nom." ".$infant->cognoms ?> s'ha inscrit.</p>
      <br/>
      <?php
        $activitats=$infant->getActivitats();
        date_default_timezone_set('Europe/Madrid');
      ?>
      <div id="veure-activitats">
        <?php foreach($activitats as $activitat){
          ?>
        <div class="infant-card">
          <h4 class="align-center"><?php echo $activitat['nom'] ?></h4>
          <div class="container">
            <div class="row">
              <div class="col-md-8 col-12 center-mobile">
                <strong>Data:</strong> <?php echo $activitat['data']; ?>
                <br/>
                <strong>Inscripció:</strong> <?php echo $activitat['registrat']; ?>
              </div>
              <div class="col-md-4 col-12 align-center">
              <?php if(DateTime::createFromFormat('Y-m-d', $activitat['data']) > DateTime::createFromFormat('m/d/Y h:i:s a',date('m/d/Y h:i:s a', time() - (30 * 24 * 60 * 60)))){ ?>
                <span class="price-tag"><?php echo $activitat['preu']; ?>€</span>
              <?php } ?>
              </div>
            </div>
          </div>
        </div>
        <br/>
      <?php } ?>
      <?php if(sizeof($activitats)==0){ ?>
          <p class="align-center text-darkgrey">Encara no s'ha inscrit a cap activitat.</p>
      <?php } ?>
      <br/>
    </div>
  </div>
</div>
