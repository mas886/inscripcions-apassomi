<?php

//We load the includes
include_once "view/includes.php";

//Generates a global variable named $con
generateConnection();

//If we have the token we will automatically create an instance of the tutor
//If the cookie doesn't exist or it's old, it will automatically be deleted
if(isset($_COOKIE['tokenAccInscrip'])){
  $GLOBALS['tutor']=new Tutor;
  $tutId=$GLOBALS['tutor']->getIdByToken($_COOKIE['tokenAccInscrip']);
  if(!$tutId){
    $GLOBALS['tutor']->cleanTokens();
    unset($_COOKIE['tokenAccInscrip']);
    setcookie('tokenAccInscrip', '', time() - 3600, '/');
  }else{
    $GLOBALS['tutor']->load($tutId);
  }
}

//we deal with ajax queries
if(isset($_GET['ajax'])){
  include_once "controller/ajax.php";
  exit;
}


//Loading normal page
?>

<!doctype html>
<html lang="ca" translate="no">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="./css/bootstrap4.1.13.min.css">
    <link rel="stylesheet" type="text/css" href="./vendor/jquery-ui-1.12.1.custom/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="./vendor/fontawesome-free-5.2.0-web/css/all.css">

    <link rel="stylesheet" type="text/css" href="./css/estil.css">
    
    <title>Inscripcions Apassomi</title>
  </head>
  <body>
    <div class="franja-head-foot head">
      <i class="fas fa-arrow-left icon-back" style="display:none;"></i>
      <h1 class="titol-head">Inscripcions<span class="beta align-center">1.0.0</span></h1>
      <i class="fas fa-sign-out-alt icon-logout" onClick="logout()" style="display:none;"></i>
    </div>
    <br/><br/><br/>
