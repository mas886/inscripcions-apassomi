<div class="container">
  <div class="row">
    <div class="col-12 col-md-6 center-div">
      <br/>
      <h2 class="align-center">Infants</h2>
      <p class="align-center">Busca l'infant a afegir</p>
      <form method="POST" id="buscar_form">
        <input class="width-100" type="text" id="nom" name="nom" placeholder="Nom (sense cognoms) *" required/>
        <input id="datanaix" class="width-100" type="text" name="datanaix" placeholder="Data de naixement *" required readonly />
        <span class="error-mes" id="error-data" style="display:none">Format incorrecte, s'accepta any-mes-dia ex: 2015-02-31</span>
        <input class="width-100" type="text" id="telefon" name="telefon" placeholder="Telèfon *" required/>
        <span class="error-mes" id="error-tel" style="display:none">Format incorrecte, evita espais i lletres.</span>
        <p class="small-text">* Escriu UN dels telèfons que va escriure a la fitxa d'inscripció del participant.</p>
        <br/>
        <button class="align-center" id="boto-buscar" type="submit">Buscar</button>
        <br/>
      </form>
      <div id="kid-container">
      </div>
    </div>
  </div>
</div>
