<br/><br/><br/>

    <div id="modal-holder" class="modal-holder" style="display:none;">
      <div class="container" style="height: 100%;">
        <div class="modal-box col-12 col-md-6">
          <p class="align-right close-modal no-margin"><i id="modal-close-button" class="fas fa-times"></i></p>
          <div id="modal-content">
          </div>
        </div>
      </div>
    </div>

    <div class="franja-head-foot footer">
      <p class="align-center">
        Suport tècnic <a href="#">info@apassomi.org</a>
        <br/>
        Software realitzat per <a href="https://scatter.cat/" target="_blank">Scatter</a>
      </p>

    </div>

    <script src="./js/jquery-3.3.1.min.js"></script>
    <script src="./vendor/jquery-ui-1.12.1.custom/jquery-ui.js"></script>
    <script src="./js/custom.js"></script>
  </body>
</html>
