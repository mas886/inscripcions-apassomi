<div class="container">
  <div class="row">
    <div class="col-12 col-md-6 center-div">
      <br/>
      <h2 class="align-center">Registre</h2>
      <p class="align-center">Dades del pare, mare o tutor</p>
      <br/>
      <form method="POST" id="register_form">
        <input class="width-100" type="text" id="nom" name="nom" placeholder="Nom *" required/>
        <input class="width-100" type="text" id="cognoms" name="cognoms" placeholder="Cognoms *" required/>
        <input class="width-100" type="text" id="dni" name="dni" placeholder="DNI *" maxlength="9" required/>
        <span class="error-mes" id="error-dni" style="display:none">DNI o NIE incorrecte comprovi errors i que no hi hagi espais.</span>
        <input id="datalogin" class="width-100" type="text" name="data" placeholder="Data de naixement *" required readonly="true"/>
        <span class="error-mes" id="error-data" style="display:none">Format incorrecte, s'accepta any-mes-dia ex: 2015-02-31</span>
        <input class="width-100" type="text" id="email" name="email" placeholder="Correu electrònic *" required/>
        <span class="error-mes" id="error-email" style="display:none">Format de l'email incorrecte</span>
        <p class="small-text">* Es important que les dades siguin verídiques, ja que es demanaran al login i per rebre la informació.</p>
        <p class="small-text" id="avislegal"><input type="checkbox" id="avislegal-check"/> Accepto la <a href="https://apassomi.org/avis-legal/" target="_blank">política de protecció de dades</a> i rebre avisos per correu electrònic.</p>
        <button class="align-center" id="boto-registre" type="submit">Registra't</button>
      </form>
      <br/>
      <br/>
    </div>
  </div>
</div>
