<?php

function getQuery($query, $variables=[]){

        $connection = $GLOBALS['$con'];
        $sql = $query;
        $sth = $connection->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $connection->setAttribute(PDO::ATTR_EMULATE_PREPARES , false); //Això serveix per a que no envie paràmetres com el LIMIT en format string i generant error.
        $sth->execute($variables);
        $resultat = $sth->fetchAll(PDO::FETCH_ASSOC);
        return $resultat;

}

function executeQuery($query, $variables = [], $getInsertId = false) {
  $connection = $GLOBALS['$con'];
  $sql = $query;
  $sth = $connection->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
  if ($getInsertId) {
      try {
          $sth->execute($variables);
          $sth->closeCursor();
      } catch(PDOException $e)  {
          return false;
      }
      if ($connection->lastInsertId() == 0) {
          return false;
      } else {
          return $connection->lastInsertId();
      }
  } else {
      try {
          $res = $sth->execute($variables);
          $sth->closeCursor();
      } catch(PDOException $e)  {
          $res=false;
      }
      return $res;
  }
}


function importFiles(){
  include_once "config.php";
  include_once "controller/functions.php";
  include_once "model/modelInfant.php";
  include_once "model/modelTutor.php";
  include_once "model/modelActivitat.php";
}

function generateConnection(){
  //db connection
  $GLOBALS['$con']=connect();

  //Don't do anything else if the db connection fails
  if(!$GLOBALS['$con']){
    echo "Error connecting db.";
    exit;
  }
}

//Will generate a global tutor object if we didn't go though the header
//If we have the token we will automatically create an instance of the tutor
//If the cookie doesn't exist or it's old, it will automatically be deleted
function generateGlobalTutor(){
  if(isset($_COOKIE['tokenAccInscrip']) && (!isset($GLOBALS['tutor']))){
    $GLOBALS['tutor']=new Tutor;
    $tutId=$GLOBALS['tutor']->getIdByToken($_COOKIE['tokenAccInscrip']);
    if(!$tutId){
      $GLOBALS['tutor']->cleanTokens();
      unset($_COOKIE['tokenAccInscrip']);
      setcookie('tokenAccInscrip', '', time() - 3600, '/');
      header("Refresh:0");
    }else{
      $GLOBALS['tutor']->load($tutId);
    }
  }
}

function connect(){

  $hostdb=hostDb;
  $userdb=userDb;
  $passdb=passDb;
  $database=nameDb;

  try{
    $connection = new PDO("mysql:host=$hostdb;dbname=$database;charset=utf8mb4", "$userdb", "$passdb", array(PDO::ATTR_PERSISTENT=>true))or die (false);
  }catch(Exception $e){
    $connection = false;
  }
  return $connection;
}

function closeConnection(){
  $connection=null;
  return $connection;
}

function replaceStringData($string, $dataArray){
  $cont=0;
  foreach($dataArray as $data){
    $string=str_replace('{'.$cont.'}', $data, $string);
    $cont++;
  }
  return $string;
}

function checkAdminLogin($usr, $pass){
  return $usr==ADMIN_USER && $pass==ADMIN_PASSWORD;
}

?>
