<?php

//Aquest importador pren les dades d'un CSV, aquest ha d'estar formatat de la següent manera
//Primera línia s'ignora
//Les següents s'organitzaran per columnes, delimitades per ;
//Els camps per ordre son:
//Nom;Cognoms;Data de Naixement (FORMAT d/m/Y 0:00:00);Telèfon Altre Contacte;Telèfon Fix;Telèfon Mobil Mare/Tutora;Telèfon Mòbil Pare/Tutor

$filename="llistats/infants-curs-2017.csv";
$myfile = fopen($filename, "r") or die("Unable to open file!");

$thingIs=explode("\n", fread($myfile,filesize($filename)));

$infant=new Infant;
unset($thingIs[0]);
foreach($thingIs as $thing){
    $explodedThing=explode(";",$thing);
    if(isset($explodedThing[0]) AND  isset($explodedThing[1]) AND isset($explodedThing[2]) AND isset($explodedThing[3]) AND isset($explodedThing[4]) AND isset($explodedThing[5]) AND isset($explodedThing[6])){
      $explodedThing[2]=implode("-",array_reverse(explode("/",explode(" ",$explodedThing[2])[0])));
      //echo $thing."___________".count($explodedThing)."<br/>";
      echo $infant->addIntoDb($explodedThing[0], $explodedThing[1], $explodedThing[2], $explodedThing[3], $explodedThing[4], $explodedThing[5], $explodedThing[6]);
    }
}

fclose($myfile);


 ?>
