<?php

if(isset($_POST['action'])){
  if ($_POST['action']=="login"){
    if(isset($_POST['dni']) && isset($_POST['data']) && $_POST['dni']!="" && $_POST['data']!=""){
      $tutor=new Tutor;
      $res=$tutor->login($_POST['dni'], $_POST['data']);
      if(!$res){
        $stat="OK";
        $disp="1";
        $mes="L'usuari no existeix, procedeixi a registrar";
      }else{
        $stat="OK";
        $disp="0";
        $mes=$res;
      }
    }else{
      $stat="ERR";
      $disp="0";
      $mes="";
    }
  }else if($_POST['action']=="registre"){
    if(isset($_POST['nom']) && isset($_POST['cognoms']) && isset($_POST['dni']) && isset($_POST['data']) && isset($_POST['email']) && $_POST['nom']!="" && $_POST['cognoms']!="" && $_POST['dni']!="" && $_POST['data']!="" && $_POST['email']!=""){
      $tutor=new Tutor;
      $res=$tutor->register($_POST['nom'], $_POST['cognoms'], $_POST['dni'], $_POST['data'], $_POST['email']);
      if(!is_string($res) && $res==true){
        $stat="OK";
        $disp="1";
        $mes=$res;
      }else{
        $stat="ERR";
        $disp="1";
        $mes=$res;
      }
    }else{
      $stat="ERR";
      $disp="0";
      $mes="";
    }

  }else if ($_POST['action']=="buscar"){
    if(isset($_POST['nom']) && isset($_POST['data']) && isset($_POST['telefon']) && $_POST['nom']!="" && $_POST['data']!="" && $_POST['telefon']!=""){
      $infant=new Infant;
      $res=$infant->searchInfant($_POST['nom'], $_POST['data'], $_POST['telefon']);
      if(is_array($res)){
        if(count($res)>0){
          $stat="OK";
          $disp="0";
          $mes=$res;
        }else{
          $stat="OK";
          $disp="1";
          $mes="No s'ha trobat l'infant";
        }
      }else{
        $stat="ERR";
        $disp="1";
        $mes="No especificat";
      }
    }else{
      $stat="ERR";
      $disp="0";
      $mes="";
    }
  }else if ($_POST['action']=="addParticipant"){
      if(isset($_POST['nom']) && isset($_POST['dataNaix']) && isset($_POST['telefon']) && $_POST['nom']!="" && $_POST['dataNaix']!="" && $_POST['telefon']!=""){
        if(isset($GLOBALS['tutor'])){
          $infant=new Infant;
          if($infant->assignInfantTutor($GLOBALS['tutor']->id, $_POST['nom'], $_POST['dataNaix'], $_POST['telefon'])){
            $stat="OK";
            $disp="1";
            $mes="S'ha assignat el participant correctament.";
          }else{
            $stat="OK";
            $disp="1";
            $mes="El participant ja està assignat o hi ha hagut un error.";
          }
        }else{
          $stat="ERR";
          $disp="0";
          $mes="";
        }
      }else{
        $stat="OK";
        $disp="1";
        $mes="Falten paràmetres.";
      }
  }else if ($_POST['action']=="inscriu"){
    if(isset($_POST['idActivitat']) && isset($_POST['idParticipant']) && $_POST['idActivitat']!="" && $_POST['idParticipant']!=""){
      if(isset($GLOBALS['tutor'])){
        $activitat=new Activitat;
        $res=$activitat->inscriureInfant($_POST['idParticipant'], $GLOBALS['tutor']->id, $_POST['idActivitat']);
        if(!is_string($res)){
          $stat="OK";
          $disp="1";
          $mes="S'ha inscrit el participant a l'activitat.";
        }else{
          $stat="ERR";
          $disp="1";
          $mes=$res;
        }
      }else{
        $stat="ERR";
        $disp="0";
        $mes="";
      }

    }
  }else if ($_POST['action']=="actualitzar"){
    if(isset($GLOBALS['tutor']) && isset($_POST['nom']) && isset($_POST['cognoms']) && isset($_POST['dni']) && isset($_POST['data']) && isset($_POST['email']) && $_POST['nom']!="" && $_POST['cognoms']!="" && $_POST['dni']!="" && $_POST['data']!="" && $_POST['email']!=""){
      if(isset($GLOBALS['tutor'])){
        $res=$GLOBALS['tutor']->update($_POST['nom'], $_POST['cognoms'], $_POST['dni'], $_POST['data'], $_POST['email']);
        if(!is_string($res)){
          $stat="OK";
          $disp="1";
          $mes="Dades modificades correctament.";
        }else{
          $stat="ERR";
          $disp="1";
          $mes=$res;
        }
      }else{
        $stat="ERR";
        $disp="0";
        $mes="";
      }
    }else{
      $stat="ERR";
      $disp="0";
      $mes="";
    }
  }else{
    $stat="ERR";
    $disp="0";
    $mes="";
  }
}else{
  $stat="ERR";
  $disp="0";
  $mes="";
}

echo json_encode(["STATUS" => $stat, "DISPLAY"=> $disp,"MESSAGE"=>$mes]);

 ?>
