$(document).ready(function() {

  loadUiFunction("login");

  if ($('#show-logout').length){
    showLogout('1');
  }

  $.datepicker.regional['cat'] = {
      closeText: 'Tancar', // set a close button text
      currentText: 'Avui', // set today text
      monthNames: ['Gener','Febrer','Març','Abril','Maig','Juny','Juliol','Agost','Setembre','Octubre','Novembre','Desembre'], // set month names
      monthNamesShort: ['Gen','Feb','Mar','Abr','Mai','Jun','Jul','Ago','Set','Oct','Nov','Des'], // set short month names
      dayNames: ['Diumenge','Dilluns','Dimarts','Dimercres','Dijous','Divendres','Dissabte'], // set days names
      dayNamesShort: ['Diu','Dil','Dima','Dime','Dijo','Div','Dis'], // set short day names
      dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Ve','Sa'], // set more short days names
      dateFormat: 'yy-mm-dd' // set format date
  };

  $.datepicker.setDefaults(
    $.extend(
      $.datepicker.regional['cat'],
      {yearRange: "-100:+0"}
    )
  )

});


var modal = document.getElementById('modal-holder');
var closeModal = document.getElementById("modal-close-button");

modal.onclick = function() {
    if($("#modal-holder").css("opacity")=="1" && !$("#modal-holder").hasClass("animacio")){
      $( "#modal-holder" ).toggle( "fade" );
      $( "#modal-holder" ).addClass('animacio');
      setTimeout(function(){
        $( "#modal-holder" ).removeClass('animacio');
      }, 1000);
    }
}

closeModal.onclick = function() {
    if($("#modal-holder").css("opacity")=="1" && !$("#modal-holder").hasClass("animacio")){
      $( "#modal-holder" ).toggle( "fade" );
      $( "#modal-holder" ).addClass('animacio');
      setTimeout(function(){
        $( "#modal-holder" ).removeClass('animacio');
      }, 1000);
    }
}

function loadUiFunction(page){
  if(page=="login"){

      $( "#datalogin" ).datepicker({
        changeMonth: true,
        changeYear: true
      });

      $("#boto-login").click(function(event){
        error=false;
        event.preventDefault();
        if($("#dni").val()==""){
          $("#dni").addClass("incorrecte");
          error=true;
        }else{
          $("#dni").removeClass("incorrecte");
        }
        if($("#datalogin").val()=="" || !checkDateFormat($("#datalogin").val())){
          $("#datalogin").addClass("incorrecte");
          $("#datalogin").val("");
          $(".error-mes").css("display","block");
          error=true;
        }else{
          $("#datalogin").removeClass("incorrecte");
          $(".error-mes").css("display","none");
        }

        if(!error){
          $.ajax({
            type: 'POST',
            url: './?ajax=ajax',
            data: {
                'action': 'login',
                'dni': $("#dni").val(),
                'data' : $("#datalogin").val()
            },
            success: function(msg){
              msg=JSON.parse(msg);
              if(msg.STATUS=="OK" && msg.DISPLAY=="1"){
                displayModal('<p>L\'usuari que has introduït es incorrecte o no està registrat.</p><p>Si està registrat, torni-ho a intentar o contacta el mail de suport.</p><button onClick="showPage(\'registre\');">Registre</button>');
              }else if(msg.STATUS=="OK" && msg.DISPLAY=="0"){
                setCookie("tokenAccInscrip", msg.MESSAGE, "30");
                showPage("panell");
              }else{
                displayModal('<p>Error al login, si el problema persisteix, contacti el suport.</p>');
              }
            }
          });
        }

      });
  }else if(page=="registre"){

      $( "#datalogin" ).datepicker({
        changeMonth: true,
        changeYear: true
      });

      $("#boto-registre").click(function(event){
        error=false;
        event.preventDefault();
        if($("#nom").val()==""){
          $("#nom").addClass("incorrecte");
          error=true;
        }else{
          $("#nom").removeClass("incorrecte");
        }
        if($("#cognoms").val()==""){
          $("#cognoms").addClass("incorrecte");
          error=true;
        }else{
          $("#cognoms").removeClass("incorrecte");
        }
        if($("#dni").val()==""){
          $("#dni").addClass("incorrecte");
          $("#error-dni").css("display","block");
          error=true;
        }else{
          $("#dni").removeClass("incorrecte");
          $("#error-dni").css("display","none");
        }
        if($("#datalogin").val()=="" || !checkDateFormat($("#datalogin").val())){
          $("#datalogin").addClass("incorrecte");
          $("#datalogin").val("");
          $("#error-data").css("display","block");
          error=true;
        }else{
          $("#datalogin").removeClass("incorrecte");
          $("#error-data").css("display","none");
        }
        if($("#email").val()=="" || !validateEmail($("#email").val())){
          $("#email").addClass("incorrecte");
          $("#error-email").css("display","block");
          error=true;
        }else{
          $("#email").removeClass("incorrecte");
          $("#error-email").css("display","none");
        }
        if(!$("#avislegal-check").is(":checked")){
          $("#avislegal").css("color","red");
          error=true;
        }else{
          $("#avislegal").css("color","inherit");
        }

        if(!error){
          $.ajax({
            type: 'POST',
            url: './?ajax=ajax',
            data: {
                'action': 'registre',
                'nom': $("#nom").val(),
                'cognoms': $("#cognoms").val(),
                'dni': $("#dni").val(),
                'data': $("#datalogin").val(),
                'email' : $("#email").val()
            },
            success: function(msg){
              msg=JSON.parse(msg);
              if(msg.STATUS!="OK" && msg.DISPLAY=="1"){
                displayModal("<p>Error: "+msg.MESSAGE+"</p>");
              }else if(msg.STATUS=="OK" && msg.DISPLAY=="1"){
                displayModal('<p>Usuari registrat correctament, accedeixi desde la pantalla de login.</p><button onClick="showPage(\'login\');">Login</button>');
                $("#boto-registre").addClass("disabled");
                $("#boto-registre").attr("disabled","disabled");
              }
            }
          });
        }

      });
  }else if(page=="infants"){
          $( "#datanaix" ).datepicker({
            changeMonth: true,
            changeYear: true
          });
          $("#boto-buscar").click(function(event){
            error=false;
            event.preventDefault();

            if($("#nom").val()==""){
              $("#nom").addClass("incorrecte");
              error=true;
            }else{
              $("#nom").removeClass("incorrecte");
            }
            if($("#datanaix").val()=="" || !checkDateFormat($("#datanaix").val())){
              $("#datanaix").addClass("incorrecte");
              $("#datanaix").val("");
              $("#error-data").css("display","block");
              error=true;
            }else{
              $("#datanaix").removeClass("incorrecte");
              $("#error-data").css("display","none");
            }
            if($("#telefon").val()=="" || !validateTelefon($("#telefon").val())){
              $("#telefon").addClass("incorrecte");
              $("#error-tel").css("display","block");
              error=true;
            }else{
              $("#telefon").removeClass("incorrecte");
              $("#error-tel").css("display","none");
            }

            if(!error){
              $("#boto-buscar").addClass("disabled");
              $("#boto-buscar").attr("disabled","disabled");
              $("#boto-buscar").html("Buscant...");
              $("#kid-container").html('<div class="align-center spinner-container"><i class="fa fa-spinner fa-spin"></i></div>');
              trobat=false;
              $.ajax({
                type: 'POST',
                  url: './?ajax=ajax',
                    data: {
                      'action': 'buscar',
                      'nom': $("#nom").val(),
                      'data': $("#datanaix").val(),
                      'telefon' : $("#telefon").val()
                    },
                    success: function(msg){
                      msg=JSON.parse(msg);
                      if(msg.STATUS=="OK" && msg.DISPLAY==1 || msg.STATUS=="ERR" && msg.DISPLAY==1){
                        displayModal("<p>"+msg.MESSAGE+"</p>");
                      }else if(msg.STATUS=="OK" && msg.DISPLAY==0){
                        trobat=true;
                        html="";
                        telefon=$("#telefon").val();
                        for(i=0;i<msg.MESSAGE.length;i++){
                          avatar=(msg.MESSAGE[0]['nom'][0]+msg.MESSAGE[0]['cognoms'][0]).toUpperCase();
                          html+=generateCard(avatar,msg.MESSAGE[0]['nom']+" "+msg.MESSAGE[0]['cognoms'], "Data Naixement: "+msg.MESSAGE[0]['data_naix'],"Afegir","claimParticipant('"+msg.MESSAGE[0]['nom']+"', '"+msg.MESSAGE[0]['data_naix']+"', '"+telefon+"')");
                        }
                        $("#kid-container").toggle("fade");
                        setTimeout(function() {
                          $("#kid-container").html(html);
                          $("#kid-container").toggle("fade");
                          $("#boto-buscar").removeClass("disabled");
                          $("#boto-buscar").removeAttr("disabled");
                          $("#boto-buscar").html("Buscar");
                          setTimeout(function() {
                            location.hash = "#kid-container";
                          }, 500);
                        }, 1000);
                      }
                    }
                  });
                  if(!trobat){
                    setTimeout(function() {
                      $("#boto-buscar").removeClass("disabled");
                      $("#boto-buscar").removeAttr("disabled");
                      $("#boto-buscar").html("Buscar");
                      $("#kid-container").html('');
                    }, 1000);
                  }
                }
          });
  }else if(page=="dades"){
          $( "#datalogin" ).datepicker({
            changeMonth: true,
            changeYear: true
          });
          $("#boto-actualitza").click(function(event){
            error=false;
            event.preventDefault();
            if($("#nom").val()==""){
              $("#nom").addClass("incorrecte");
              error=true;
            }else{
              $("#nom").removeClass("incorrecte");
            }
            if($("#cognoms").val()==""){
              $("#cognoms").addClass("incorrecte");
              error=true;
            }else{
              $("#cognoms").removeClass("incorrecte");
            }
            if($("#dni").val()==""){
              $("#dni").addClass("incorrecte");
              $("#error-dni").css("display","block");
              error=true;
            }else{
              $("#dni").removeClass("incorrecte");
              $("#error-dni").css("display","none");
            }
            if($("#datalogin").val()=="" || !checkDateFormat($("#datalogin").val())){
              $("#datalogin").addClass("incorrecte");
              $("#datalogin").val("");
              $("#error-data").css("display","block");
              error=true;
            }else{
              $("#datalogin").removeClass("incorrecte");
              $("#error-data").css("display","none");
            }
            if($("#email").val()=="" || !validateEmail($("#email").val())){
              $("#email").addClass("incorrecte");
              $("#error-email").css("display","block");
              error=true;
            }else{
              $("#email").removeClass("incorrecte");
              $("#error-email").css("display","none");
            }

            if(!error){
              $.ajax({
                type: 'POST',
                url: './?ajax=ajax',
                data: {
                    'action': 'actualitzar',
                    'nom': $("#nom").val(),
                    'cognoms': $("#cognoms").val(),
                    'dni': $("#dni").val(),
                    'data': $("#datalogin").val(),
                    'email' : $("#email").val()
                },
                success: function(msg){
                  msg=JSON.parse(msg);
                  if(msg.STATUS!="OK" && msg.DISPLAY=="1"){
                    displayModal("<p>Error: "+msg.MESSAGE+"</p>");
                  }else if(msg.STATUS=="OK" && msg.DISPLAY=="1"){
                    displayModal('<p>Usuari actualitzat correctament.</p>');
                  }
                }
              });
            }



          });

  }
}

function showHideDiv(sowId, hideId){
    if(!$("#"+sowId).hasClass("animacio") && $("#"+sowId).css("display")=="none"){
      $("#"+sowId).addClass("animacio");
      $("#"+hideId).toggle( "fade" );
      setTimeout(function() {
        $("#"+sowId).toggle( "fade" );
        setTimeout(function() {
          $("#"+sowId).removeClass("animacio");
        }, 1000);
      }, 300);
    }
}


var paginesBotoEnrere={'registre':'login','activitats':'panell','dades':'panell', 'infants':'panell', 'inscripcions':'panell'};
var paginesBotoLogout={'infants':'','activitats':'','panell':'','dades':'','inscripcions':''};

function searchButton(needle, arrhaystack){
  for($key in arrhaystack){
    if($key==needle){
      return true;
    }
  }
  return false;
}

function showPage(page, params=""){
  if(!$("#main").hasClass("animacio")){
    if(searchButton(page, paginesBotoEnrere)){
      setBackButton('1',paginesBotoEnrere[page]);
    }else{
      setBackButton('0',"");
    }

    if(searchButton(page, paginesBotoLogout)){
      showLogout('1');
    }else{
      showLogout('0');
    }
    $.ajax({
      type: 'POST',
      url: './controller/loadPage.php',
      data: {
        'pageName':page,
        'params':params
      },
      success: function(msg){
        $("#main").addClass("animacio");
        $( "#main" ).toggle( "fade" );
        var x = 1;
        var y = null; // To keep under proper scope

        setTimeout(function() {
              $("#main").html(msg);
              loadUiFunction(page);
              $( "#main" ).toggle( "fade" );
                $("#main").removeClass("animacio");
        }, 400);
      }
    });
  }
}

function showLogout(visibility){
  if(visibility==1 && $(".icon-logout").css("display")=="none"){
    $(".icon-logout").toggle("fade");
  }else if((visibility==0 && $(".icon-logout").css("display")!="none")){
    $(".icon-logout").toggle("fade");
  }
}

function logout(){
  setCookie('tokenAccInscrip','',-30);
  showPage('login');
}

function setBackButton(visibility,page){
  if(visibility==1 && $(".icon-back").css("display")=="none"){
    $(".icon-back").toggle("fade");
    $(".icon-back").attr("onClick","showPage('"+page+"')");
  }else if((visibility==0 && $(".icon-back").css("display")!="none")){
    $(".icon-back").toggle("fade");
    $(".icon-back").attr("onClick","showPage('"+page+"')");
  }
}

function inscriuActivitatParticipant(idActivitat, idParticipant, idButton){
  $("#"+idButton).addClass('disabled');
  $("#"+idButton).attr('disabled','disabled');
  $("#"+idButton).html('<i class="fas fa-spinner fa-spin"></i> Processant...');
  $.ajax({
    type: 'POST',
    url: './?ajax=ajax',
    data: {
        'action': 'inscriu',
        'idActivitat': idActivitat,
        'idParticipant': idParticipant
    },
    success: function(msg){
      msg=JSON.parse(msg);
      if(msg.STATUS=="OK" && msg.DISPLAY=="1"){
        displayModal("<p>"+msg.MESSAGE+"</p>");
        showPage('activitats',idParticipant);
      }else if (msg.STATUS=="ERR" && msg.DISPLAY=="1") {
        displayModal("<p>"+msg.MESSAGE+"</p>");
      }
    }
  });
}

function claimParticipant(nom, dataNaix, telefon){

  $("#infant-card").addClass('disabled');
  $("#infant-card").attr('disabled','disabled');
  $("#infant-card").html('<i class="fas fa-spinner fa-spin"></i> Processant...');
  $.ajax({
    type: 'POST',
    url: './?ajax=ajax',
    data: {
        'action': 'addParticipant',
        'nom': nom,
        'dataNaix': dataNaix,
        'telefon': telefon
    },
    success: function(msg){
      msg=JSON.parse(msg);
      if(msg.STATUS=="OK" && msg.DISPLAY=="1"){
        displayModal("<p>"+msg.MESSAGE+"</p>");
      }
      $("#infant-card").html('Fet');
    },
      error: function (xhr, ajaxOptions, thrownError) {
        alert(xhr.status);
        alert(thrownError);
        $("#infant-card").html('Fet');
      }
  });
}

function generateCard(avatar,name,text,button,funct){
  html='<div class="infant-card"><div class="avatar">'+avatar+'</div><p class="card-nom">'+name+'</p><br/><div class="container"><div class="row"><div class="col-md-6 col-12">'+
  '<p>'+text+'</p></div><div class="col-md-6 col-12"><button id="infant-card" class="button-100" onClick="'+funct+'">'+button+'</button></div></div></div></div><br/>';
  return html;
}

function displayModal(contingut){
$("#modal-content").html(contingut);
$("#modal-holder").toggle( "fade" );
}

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

function validateTelefon(telefono){
    var patro = /^([0-9\+\s\+\-]{9,})$/;
    return patro.test(telefono);
}

function checkDateFormat(date){
  var date_regex = /^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/ ;
  if(!(date_regex.test(date))){
    return false;
  }else{
    return true;
  }
}

function setCookie(name,value,days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
}
