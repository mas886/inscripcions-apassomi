<?php

use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;

include_once $isJson . "vendor/PHPMailer/src/Exception.php";
include_once $isJson . "vendor/PHPMailer/src/PHPMailer.php";
include_once $isJson . "vendor/PHPMailer/src/SMTP.php";

class Mail
{

    public function sendMailInscripcio($tutorNom, $participantNom, $activitatNom, $dataActivitat, $preuActivitat, $tutorsMails){
        $mail = new PHPMailer(true); // Passing `true` enables exceptions
        try {
            //Server settings
            $mail->SMTPDebug = 0;
            $mail->isSMTP();
            $mail->Host = SMTP_SERVER;
            $mail->SMTPAuth = true;
            $mail->Username = SMTP_USER;
            $mail->Password = SMTP_PASSWORD;
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;
            
            //Recipients
            $mail->setFrom(SMTP_USER, "Grup d'Esplai Apassomi - No Contestar");
            foreach($tutorsMails as $tutor){
                $mail->AddBCC($tutor['correu'], $tutor['nom']);
            }

            //Content
            $mail->CharSet = 'UTF-8';
            $mail->isHTML(true);
            $mail->Subject = "Avís d'inscripció de $participantNom a $activitatNom";
            $mail->Body = replaceStringData(file_get_contents('plantilles/inscripcioMail.html'),[$tutorNom,$participantNom,$activitatNom,$dataActivitat,number_format((float)$preuActivitat, 2, '.', ''), date('d-m-Y h:i:s a', time())]);
            $mail->AltBody = replaceStringData(file_get_contents('plantilles/inscripcioMail.html'),[$tutorNom,$participantNom,$activitatNom,$dataActivitat,number_format((float)$preuActivitat, 2, '.', ''), date('d-m-Y h:i:s a', time())]);

            $mail->send();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function sendMailVinculacioParticipant($tutorNom, $participantNom, $tutorsMails){
        $mail = new PHPMailer(true); // Passing `true` enables exceptions
        try {
            //Server settings
            $mail->SMTPDebug = 0;
            $mail->isSMTP();
            $mail->Host = SMTP_SERVER;
            $mail->SMTPAuth = true;
            $mail->Username = SMTP_USER;
            $mail->Password = SMTP_PASSWORD;
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;
            
            //Recipients
            $mail->setFrom(SMTP_USER, "Grup d'Esplai Apassomi - No Contestar");
            foreach($tutorsMails as $tutor){
                $mail->AddBCC($tutor['correu'], $tutor['nom']);
            }

            //Content
            $mail->CharSet = 'UTF-8';
            $mail->isHTML(true);
            $mail->Subject = "Avís, $tutorNom ha vinculat a $participantNom al seu compte.";
            $mail->Body = replaceStringData(file_get_contents('plantilles/afegitParticipantMail.html'),[$tutorNom,$participantNom, date('d-m-Y h:i:s a', time())]);
            $mail->AltBody = replaceStringData(file_get_contents('plantilles/afegitParticipantMail.html'),[$tutorNom,$participantNom, date('d-m-Y h:i:s a', time())]);

            $mail->send();
            return true;
        } catch (Exception $e) {
            return false;
        }

    }

}
