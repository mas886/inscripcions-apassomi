<?php

class Activitat{

  private $id;
  private $nom;
  private $preu;
  private $visible;
  private $data;
  private $inici_inscripcions;
  private $final_inscripcions;
  private $data_creacio;


  function __construct($id="") {
    if($id!=""){
      $this->load($id);
    }
  }

  public function load($id){
    $query="SELECT * FROM `activitat` WHERE `id`=:id";
    $result=getQuery($query,[":id"=>$id]);
    if (count($result) != 0){
      foreach($result[0] as $key=>$value){
          $this->set_field($key, $value);
      }
      return true;
    }else{
      return false;
    }
  }

  public function inscriureInfant($infantId, $tutorId, $activitatId){
    $infant=new Infant($infantId);
    $this->load($activitatId);
    if($infant->belongsToTutor($tutorId,$infantId)){
      if($this->isActive($activitatId) && $this->checkInfantActivitat($infantId, $activitatId)){
        $query="INSERT INTO `infant_activitat` (`infant_id`, `activitat_id`, `tutor_id`) VALUES (:infantId, :activitatId, :tutorId);";
        if(executeQuery($query,[":infantId"=>$infantId,":activitatId"=>$activitatId,":tutorId"=>$tutorId])){
          $mail=new Mail;
          $mail->sendMailInscripcio($GLOBALS['tutor']->nom.' '.$GLOBALS['tutor']->cognoms, $infant->nom.' '.$infant->cognoms, $this->nom, $this->data, $this->preu, $infant->getCorreusTutors());
          return true;
        }else{
          return "El participant ja està inscrit a l'activitat o s'ha produit un error.";
        }
      }else{
        return "L'activitat ja no està disponible.";
      }
    }else{
      return "L'infant no correspon al tutor.";
    }
  }

  public function checkInfantActivitat($infantId, $activitatId){
    //Aquesta funció compara el grups als quals està el participant i els grups als quals està l'activitat, en cas que hi hagi coincidència retorna verdader
    $query= "SELECT grup_id FROM `infant_grup` WHERE `infant_id` = :infantId";
    $grupsInfant = [];
    foreach(getQuery($query,[":infantId"=>$infantId]) as $grupI){
      array_push($grupsInfant, $grupI['grup_id']);
    }
    $query= "SELECT grup_id FROM `activitat_grup` WHERE`activitat_id` = :activitatId";
    $grupsActivitat = [];
    foreach(getQuery($query,[":activitatId"=>$activitatId]) as $grupA){
      array_push($grupsActivitat, $grupA['grup_id']);
    }
    if (count(array_intersect($grupsActivitat, $grupsInfant)) > 0) {
      return true;
    }else{
      return false;
    }
  }

  public function isActive($id=""){
    if($id=="")
      $id=$this->id;
    $query="SELECT id FROM `activitat` WHERE `inici_inscripcions`<= CURRENT_DATE AND `final_inscripcions`>= CURRENT_DATE AND visible = 1 AND id=:id";
    $result=getQuery($query,[":id"=>$id]);
    if (count($result) != 0){
      return true;
    }else{
      return false;
    }
  }

  public function getActivitats($visible=1){
    if($visible==1){
      $query = "SELECT `id`,`nom`, `preu`, `data`, `inici_inscripcions`,`final_inscripcions` FROM `activitat` WHERE `inici_inscripcions`<= CURRENT_DATE AND `final_inscripcions`>= CURRENT_DATE AND visible = 1";
    }else{
      $query="SELECT `id`,`nom`, `preu`, `data`, `inici_inscripcions`, `final_inscripcions` FROM `activitat`";
    }
    return getQuery($query);
  }

  public function getActivitatsParticipant($participantId, $visible=1){
    //Tant el grup com l'activitat han d'estar visibles
    if($visible==1){
      $query = "SELECT DISTINCT ig.grup_id, g.nom grup_nom, a.id, a.nom, a.preu, a.data, a.inici_inscripcions, a.final_inscripcions, a.data_creacio FROM `infant_grup` ig JOIN activitat_grup ag ON ag.grup_id = ig.grup_id JOIN grup g ON g.id = ag.grup_id JOIN activitat a ON a.id = activitat_id WHERE ig.infant_id=:infantId AND `inici_inscripcions`<= CURRENT_DATE AND `final_inscripcions`>= CURRENT_DATE AND a.visible = 1 AND g.visible = 1;";
    }else{
      $query="SELECT DISTINCT ig.grup_id, g.nom grup_nom, a.id, a.nom, a.preu, a.data, a.inici_inscripcions, a.final_inscripcions, a.data_creacio FROM `infant_grup` ig JOIN activitat_grup ag ON ag.grup_id = ig.grup_id JOIN grup g ON g.id = ag.grup_id JOIN activitat a ON a.id = activitat_id WHERE ig.infant_id=:infantId AND `inici_inscripcions`<= CURRENT_DATE AND `final_inscripcions`>= CURRENT_DATE";
    }
    $res = getQuery($query,[':infantId'=>$participantId]);
    $alreadyInTable=[];
    $returnQueries=[];
    //Això evita activitats duplicades quan l'activitat està associada a més d'un grup dels que el participant forma part
    foreach($res as $activitat){
      if(!in_array($activitat['id'],$alreadyInTable)){
        array_push($returnQueries, $activitat);
        array_push($alreadyInTable, $activitat['id']);
      }
    }
    return $returnQueries;
  }

  function set_field($field, $value) {
    $this->$field = $value;
  }

}
