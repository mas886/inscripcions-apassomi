<?php

class Tutor{

  public $id;
  public $nom;
  public $cognoms;
  public $dni;
  public $data_naix;
  public $correu;
  public $data_alta;

  function __construct($id="") {
    if($id!=""){
      $this->load($id);
    }
  }

  function load($id){
    $query="SELECT * FROM tutor WHERE id = :id ;";
    $result=getQuery($query,[":id"=>$id]);
    if (count($result) != 0){
      foreach($result[0] as $key=>$value){
          $this->set_field($key, $value);
      }
      return true;
    }else{
      return false;
    }
  }

  function login($dni, $data_naix){
    $query="SELECT id FROM tutor WHERE dni = :dni AND data_naix = :data_naix ;";
    $result=getQuery($query,[":dni"=>$dni, ":data_naix"=>$data_naix]);
    if (count($result) != 0){
      $this->load($result[0]['id']);
      $token=$this->createToken();
      return $token;
    }else{
      return false;
    }
  }

  function register($nom, $cognoms, $dni, $data_naix, $correu){
    if($this->checkDni($dni)){
      if(!$this->exist($dni)){
        //Comprovem que el DNI sigui correcte
        $query="INSERT INTO `tutor`(`nom`, `cognoms`, `dni`, `data_naix`, `correu`) VALUES (:nom, :cognoms, :dni, :datanaix, :correu)";
        if(!executeQuery($query,[":nom"=>$nom,":cognoms"=>$cognoms,":dni"=>$dni,":datanaix"=>$data_naix,":correu"=>$correu])){
          return "Error no especificat.";
        }else{
          return true;
        }
      }else{
        return "Aquest DNI ja està registrat.";
      }
    }else{
      return "El DNI es incorrecte.";
    }
  }

  function update($nom, $cognoms, $dni, $dataNaix, $correu, $id=""){
    if($id==""){
      $id=$this->id;
    }
    //Comprovem que el DNI sigui correcte
    if($this->checkDni($dni)){
      if($this->exist($dni) && $dni!=$this->dni){
        return "Aquest DNI ja està registrat.";
      }else{
          $query = "UPDATE `tutor` SET `nom`=:nom,`cognoms`=:cognoms,`dni`=:dni,`data_naix`=:data,`correu`=:correu WHERE `id`=:id";
          if(!executeQuery($query, [":id"=>$this->id,":nom"=>$nom,":cognoms"=>$cognoms,":dni"=>$dni,":data"=>$dataNaix,":correu"=>$correu])){
            return "Error no especificat.";
          }else{
            return true;
          }
      }
    }else{
        return "El DNI es incorrecte.";
      }
  }

  function getInfants($id=""){
    if($id=="") $id=$this->id;
    if($id==NULL){
      return false;
    }else{
      $query="SELECT `infant_id`, `tutor_id` FROM `infant_tutor` WHERE `tutor_id`=:id";
      $result=getQuery($query,[":id"=>$id]);
      $infants=[];
      foreach($result as $infant){
        $infants[]=new Infant($infant['infant_id']);
      }
      return $infants;
    }
  }

  function exist($dni){
      $dni=strtoupper($dni);
      $query="SELECT dni FROM tutor WHERE dni = :dni;";
      $result=getQuery($query,[":dni"=>$dni]);
      if (count($result) != 0){
        return true;
      }else{
        return false;
      }
  }


  function createToken() {
     //This prevents generating duplicate tokens
     do {
         $token = bin2hex(random_bytes(15));
     } while ($this->tokenExist($token));
     //Clean old tokens from the user
     $this->cleanTokens();
     //Insert new token
     $this->addToken($token);
     return $token;
   }

   function addToken($token){
     $query = "INSERT INTO `login_token`(`tutor_id`, `token`, `data_expira`) VALUES (:tutor_id, :token,DATE_ADD(CURRENT_TIMESTAMP, INTERVAL 30 DAY))";
     executeQuery($query, [":tutor_id"=>$this->id,":token"=>$token]);
   }

   function cleanTokens(){
     $query="DELETE FROM `login_token` WHERE `data_expira` < CURRENT_TIMESTAMP";
     executeQuery($query);
   }

   function getIdByToken($token){
     $query="SELECT tutor_id FROM login_token WHERE token = :token AND data_expira > CURRENT_TIMESTAMP;";
     $result=getQuery($query,[":token"=>$token]);
     if (count($result) != 0)
      return $result[0]['tutor_id'];
    else
      return false;
   }

   function tokenExist($token){
     $query="SELECT token FROM login_token WHERE token = :token;";
     $result=getQuery($query,[":token"=>$token]);
     if (count($result) != 0)
      return true;
    else
      return false;
   }

   function checkDni($dni){
     if(strlen($dni)<9) {
    		return false;
    	}

    	$dni = strtoupper($dni);

    	$lletra = substr($dni, -1, 1);
    	$num = substr($dni, 0, 8);

      if(!is_numeric($num[0]) && !in_array($num[0],['X', 'Y', 'Z'])){
        return false;
      }

    	$num = str_replace(['X', 'Y', 'Z'], array(0, 1, 2), $num);
      if(!is_numeric($num)){
        return false;
      }
    	if(substr("TRWAGMYFPDXBNJZSQVHLCKE", $num % 23, 1)!=$lletra) {
    		return false;
    	}
    	else {
    		return true;
    	}
   }

   function set_field($field, $value) {
       $this->$field = $value;
   }

}

 ?>
