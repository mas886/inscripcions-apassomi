<?php

class Infant{

  public $id;
  public $nom;
  public $cognoms;
  public $data_naix;
  public $telefon_altre;
  public $telefon_fix;
  public $telefon_mare;
  public $telefon_pare;
  public $data_alta;

  function __construct($id="") {
    if($id!=""){
      $this->load($id);
    }
  }

  public function load($id){
    $query="SELECT * FROM `infant` WHERE `id`=:id";
    $result=getQuery($query,[":id"=>$id]);
    if (count($result) != 0){
      foreach($result[0] as $key=>$value){
          $this->set_field($key, $value);
      }
      return true;
    }else{
      return false;
    }
  }

  public function searchInfant($nom, $dataNaix, $telefon){
    $telefon=str_replace(" ","",$telefon);
    if($telefon!="" AND $nom!="" AND $dataNaix!=""){
      $query='SELECT `id`, `nom`, `cognoms`, `data_naix` FROM `infant` WHERE  LOWER (`nom`) LIKE :nom AND data_naix = :dataNaix AND (REPLACE(`telefon_altre`," ", "")=:telefon1 OR REPLACE(`telefon_fix`," ", "")=:telefon2 OR REPLACE(`telefon_mare`," ", "")=:telefon3 OR REPLACE(`telefon_pare`," ", "")=:telefon4)';
      $result=getQuery($query,[":nom"=>"".trim(strtolower($nom))."",":dataNaix"=>$dataNaix,":telefon1"=>$telefon,":telefon2"=>$telefon,":telefon3"=>$telefon,":telefon4"=>$telefon]);
      return $result;
    }else{
      return false;
    }
  }

  public function getCorreusTutors($infantId=""){
    if($infantId==""){
      $infantId=$this->id;
    }
    $query='SELECT CONCAT(t.nom," ",t.cognoms) as nom, t.correu as correu FROM `infant_tutor` it 
      JOIN tutor t on it.tutor_id=t.id 
      WHERE infant_id=:infantId';
    $result=getQuery($query,[":infantId"=>$infantId]);
    return $result;
  }

  public function getActivitats($infantId=""){
    if($infantId==""){
      $infantId=$this->id;
    }
    $query="SELECT `infant_id`, a.id,a.nom, a.data, a.preu, ia.`data_inscripció`, CONCAT(t.nom,' ',t.cognoms) as 'registrat' FROM `infant_activitat` ia
      JOIN `activitat` a ON a.id=ia.activitat_id
      JOIN `tutor` t ON t.id = ia.tutor_id
      WHERE infant_id=:infantId AND a.visible=1
      ORDER BY data DESC;";
    $result=getQuery($query,[":infantId"=>$infantId]);
    return $result;
  }

  public function belongsToTutor($tutorId, $infantId=""){
    if($infantId==""){
      $infantId=$this->id;
    }
    $query="SELECT `infant_id`, `tutor_id` FROM `infant_tutor` WHERE `infant_id`= :infantId AND `tutor_id`=:tutorId; ";
    $result=getQuery($query,[":infantId"=>$infantId, ":tutorId"=>$tutorId]);
    if (count($result) != 0){
      return true;
    }else{
      return false;
    }

  }

  public function assignInfantTutor($tutorId, $nom, $dataNaix, $telefon){
    //Ens assegurem que tinguem coneixement de la informació de l'infant
    $infant=$this->searchInfant($nom, $dataNaix, $telefon);
    if(!$infant || count($infant)==0){
      return false;
    }else{
      $query="INSERT INTO `infant_tutor`(`infant_id`, `tutor_id`) VALUES (:infantId, :tutorId);";
      $result=executeQuery($query, [":infantId"=>$infant[0]['id'],":tutorId"=>$tutorId]);
      $infant=$this->load($infant[0]['id']);
      if($result){
        $mail=new Mail;
        $mail->sendMailVinculacioParticipant($GLOBALS['tutor']->nom.' '.$GLOBALS['tutor']->cognoms,  $this->nom.' '.$this->cognoms, $this->getCorreusTutors());
      }
      return $result;
    }

  }

  public function addIntoDb($nom, $cognoms, $dataNaix, $telAlt, $telFix, $telMare, $telPare){
    $dataNaix=date_format(date_create($dataNaix),"Y-m-d");
    $query="SELECT id FROM `infant` WHERE data_naix = :dataNaix AND nom = :nom AND cognoms = :cognoms ;";
    $result=getQuery($query,[":dataNaix"=>$dataNaix, ":nom"=>$nom, ":cognoms"=>$cognoms]);
    if (count($result) == 0){
      $query="INSERT INTO `infant`( `nom`, `cognoms`, `data_naix`, `telefon_altre`, `telefon_fix`, `telefon_mare`, `telefon_pare`)
      VALUES (:nom, :cognoms, :dataNaix, :telAlt, :telnFix, :telMare, :telPare);";
      return executeQuery($query,[":nom"=>$nom,":cognoms"=>$cognoms,":dataNaix"=>$dataNaix,":telAlt"=>$telAlt,":telnFix"=>$telFix,":telMare"=>$telMare,":telPare"=>$telPare], true);
    }else{
      $query ="UPDATE `infant` SET `telefon_altre`=:telAlt,`telefon_fix`=:telnFix,`telefon_mare`=:telMare,`telefon_pare`=:telPare WHERE data_naix = :dataNaix AND nom = :nom AND cognoms = :cognoms ;";
      executeQuery($query,[":nom"=>$nom,":cognoms"=>$cognoms,":dataNaix"=>$dataNaix,":telAlt"=>$telAlt,":telnFix"=>$telFix,":telMare"=>$telMare,":telPare"=>$telPare]);
      return $result[0]['id'];
    }
  }

  public function addIntoDbProv($nom, $cognoms, $dataNaix, $telAlt, $telFix, $telMare, $telPare, $remesaId, $activitatId=""){
    $dataNaix=date_format(date_create($dataNaix),"Y-m-d");
    $query="SELECT nom FROM `infant` WHERE data_naix = :dataNaix AND nom = :nom AND cognoms = :cognoms ;";
    $result=getQuery($query,[":dataNaix"=>$dataNaix, ":nom"=>$nom, ":cognoms"=>$cognoms]);
    if (count($result) == 0){
      $action="Nou";
    }else{
      $action="Actualitzar";
    }
    $query="INSERT INTO `infant_provisional`( `nom`, `cognoms`, `data_naix`, `telefon_altre`, `telefon_fix`, `telefon_mare`, `telefon_pare`, `remesaId`, `action`, `grupId`)
    VALUES (:nom, :cognoms, :dataNaix, :telAlt, :telnFix, :telMare, :telPare, :remesaId, :action, :activitatId);";
    $res=executeQuery($query,[":nom"=>$nom,":cognoms"=>$cognoms,":dataNaix"=>$dataNaix,":telAlt"=>$telAlt,":telnFix"=>$telFix,":telMare"=>$telMare,":telPare"=>$telPare,":remesaId"=>$remesaId,":action"=>$action,':activitatId'=>$activitatId]);
    return $res;
  }

  public static function deleteProvOld(){
    //Per evitar la sobrecàrrega de la base de dades, esborra de la taula provisional les entrades més antigues de 30 dies.
    $query="DELETE FROM `infant_provisional` WHERE remesaId < UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL 30 DAY))";
    return executeQuery($query);
  }

  public static function assignGrup($participantId, $grupId){
    //A priori, aquesta comprovació no faria falta, ja que la base de dades mateix defineix els dos camps com a clau única, però més seguretat.
    if(!Infant::isInGrup($participantId, $grupId)){
      $query="INSERT INTO `infant_grup`(`infant_id`, `grup_id`) VALUES (:infantId,:grupId)";
      $res=executeQuery($query,[':infantId'=>$participantId,':grupId'=>$grupId]);
      return $res;
    }else{
      return false;
    }
  }

  public static function unassignGrup($participantId, $grupId){
    if(Infant::isInGrup($participantId, $grupId)){
      $query = "DELETE FROM `infant_grup` WHERE `infant_id` = :infantId AND `grup_id` = :grupId";
      return executeQuery($query, [':infantId' => $participantId, ':grupId' => $grupId]);
    }else{
      return false;
    }
  }

  public static function isInGrup($participantId, $grupId){
    $query = "SELECT * FROM `infant_grup` WHERE `infant_id` = :infantId AND `grup_id` = :grupId";
    $result=getQuery($query,[":infantId"=>$participantId, ":grupId"=>$grupId]);
    if (count($result) == 0){
      return false;
    }else{
      return true;
    }
  }

  function set_field($field, $value) {
    $this->$field = $value;
  }

}
